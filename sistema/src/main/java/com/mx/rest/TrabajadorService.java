package com.mx.rest;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


import org.hibernate.Session;
import org.hibernate.Transaction;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mx.configuraciones.HibernateUtil;
import com.mx.entidades.Trabajadores;
import com.mx.mapeadojackson.Trabajadoresjackson;

@Path("/Trabajador")
public class TrabajadorService implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TrabajadorService() {
		// TODO Auto-generated constructor stub
	}
	
	@GET
	@Path("/trabajador")
	public String mostrarjson() {
		Trabajadoresjackson trabajador = new Trabajadoresjackson();
		trabajador.setId_trabajador(1);
		trabajador.setNombre_trabajador("JOSE MANUEL");
		trabajador.setApellidop_trabajador("ALvarez");
		trabajador.setApellidom_trabajador("Bucio");
		trabajador.setId_suc(3);
		trabajador.setId_tipo(1);
		try {

			ObjectMapper mapper = new ObjectMapper();

			String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(trabajador);
			return json;
		} catch (Exception e) {
			// TODO: handle exception
		}

		return "NOOOO";
	}
	
	@POST
	@Path("/new/")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addtrabajador(Trabajadoresjackson trabajadorjson) {
		Session sessiontrabajador = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = sessiontrabajador.beginTransaction();
		try {
			Trabajadores trabajador = new Trabajadores();
			if (trabajadorjson.getNombre_trabajador()!= null) {
				trabajador.setNombre_trabajador(trabajadorjson.getNombre_trabajador());
			}
			if (trabajadorjson.getApellidop_trabajador() != null) {
				trabajador.setApellidop_trabajador(trabajadorjson.getApellidop_trabajador());
			}
			if (trabajadorjson.getApellidom_trabajador() != null) {
				trabajador.setApellidom_trabajador(trabajadorjson.getApellidom_trabajador());
			}
			if (trabajadorjson.getPassword()!=null) {
				trabajador.setPassword(trabajadorjson.getPassword() );
			}
			if (trabajadorjson.getId_suc() >0) {
				trabajador.setId_suc(trabajadorjson.getId_suc() );
			}
			if (trabajadorjson.getId_tipo() >0) {
				trabajador.setId_tipo(trabajadorjson.getId_tipo() );
			}
			trabajador.setCreate_date(ObtenerUTC());
			trabajador.setUpdate_date(ObtenerUTC());
			trabajador.setEnable(true);
			sessiontrabajador.save(trabajador);
			tx.commit();
			return Response.status(200).entity("Oepracion exitosa!!").build();
		} catch (RuntimeException e) {
			// TODO: handle exception
			e.printStackTrace();
			return Response.status(200).entity("Error: " + e).build();
		}

	}
	


	@PUT
	@Path("/update/")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateTrabajador(Trabajadoresjackson trabajadorjson) {
		Session sessiontrabajador = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = sessiontrabajador.beginTransaction();
		try {
			ObjectMapper mapper = new ObjectMapper();
			String jsonStr = "";
			Trabajadores trabajador = (Trabajadores) sessiontrabajador.get(Trabajadores.class, trabajadorjson.getId_trabajador());
			if (trabajador != null) {
				if (trabajador.getEnable()) {
					if (trabajadorjson.getNombre_trabajador()!= null) {
						trabajador.setNombre_trabajador(trabajadorjson.getNombre_trabajador());
					}
					if (trabajadorjson.getApellidop_trabajador() != null) {
						trabajador.setApellidop_trabajador(trabajadorjson.getApellidop_trabajador());
					}
					if (trabajadorjson.getApellidom_trabajador() != null) {
						trabajador.setApellidom_trabajador(trabajadorjson.getApellidom_trabajador());
					}
					if (trabajadorjson.getPassword()!=null) {
						trabajador.setPassword(trabajadorjson.getPassword() );
					}
					if (trabajadorjson.getId_suc() >0) {
						trabajador.setId_suc(trabajadorjson.getId_suc() );
					}
					if (trabajadorjson.getId_tipo() >0) {
						trabajador.setId_tipo(trabajadorjson.getId_tipo() );
					}
					trabajador.setUpdate_date(ObtenerUTC());
					trabajador.setEnable(true);
					jsonStr += mapper.writerWithDefaultPrettyPrinter().writeValueAsString(trabajador);
					tx.commit();
					return Response.status(200).entity(jsonStr).build();
				} else {
					return Response.status(200).entity("No se puedo efectuar la operacion ").build();
				}
			} else {
				return Response.status(200).entity("No se encontro trabajador con los datos ingresados ").build();
			}
		} catch (RuntimeException e) {
			e.printStackTrace();
			if ((tx != null) && (tx.isActive())) {
				tx.rollback();
			}
			return Response.status(200).entity("Error: " + e).build();
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return Response.status(200).entity("No se puedo efectuar la operacion ").build();
	}
	
	
	@GET
	@Path("/search/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response searchtrabajador(@PathParam("id") long id) {
		Session sessiontrabajador = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = sessiontrabajador.beginTransaction();
		try {
			ObjectMapper mapper = new ObjectMapper();
			String jsonStr = "";
			Trabajadores trabajador = (Trabajadores) sessiontrabajador.get(Trabajadores.class, id);
			if (trabajador != null) {

				if (trabajador.getEnable()) {
					jsonStr += mapper.writerWithDefaultPrettyPrinter().writeValueAsString(trabajador);
					return Response.status(200).entity("" + jsonStr).build();
				} else {
					return Response.status(200).entity("No se puede efectuar esta operacion").build();
				}
			}else {
				return Response.status(200).entity("Error ....").build();
			}
		} catch (RuntimeException e) {
			e.printStackTrace();
			if ((tx != null) && (tx.isActive())) {
				tx.rollback();
			}
			return Response.status(200).entity("Error: " + e).build();
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return Response.status(200).entity("Error ....").build();
	}

	
	@DELETE
	@Path("/delete/{id}")
	public Response deletetrabajador(@PathParam("id") long id) {
		Session sessiontrabajador = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = sessiontrabajador.beginTransaction();
		try {
			Trabajadores trabajador = (Trabajadores) sessiontrabajador.get(Trabajadores.class, id);
			if (trabajador != null) {
				if (trabajador.getEnable()) {
					trabajador.setEnable(false);
					tx.commit();
					return Response.status(200).entity("Oepracion exitosa!!").build();
				} else {
					return Response.status(200).entity("No se pudo efectuar la operacion").build();
				}
			} else {
				return Response.status(200).entity("Error...!!").build();
			}
		} catch (RuntimeException e) {
			e.printStackTrace();
			return Response.status(200).entity("Error: " + e).build();
		}
	}
	
	public static String ObtenerUTC() {
		final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
		final String utcTime = sdf.format(new Date());

		return utcTime;
	}

}
