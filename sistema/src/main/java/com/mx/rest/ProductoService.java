package com.mx.rest;


import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mx.configuraciones.HibernateUtil;
import com.mx.entidades.Producto;
import com.mx.mapeadojackson.Productojackson;

@Path("/Producto")
public class ProductoService implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ProductoService() {
		// TODO Auto-generated constructor stub
	}
	
	@POST
	@Path("/new/")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addProducto(Productojackson productojson) {
		Session sessionproducto = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = sessionproducto.beginTransaction();
		try {
			Producto producto = new Producto();
			if (productojson.getNombre() != null) {
				producto.setNombre(productojson.getNombre());
			}
			if (productojson.getDescripcion() != null) {
				producto.setDescripcion(productojson.getDescripcion());
			}
			if (productojson.getPrecio_compra() > 0) {
				producto.setPrecio_compra(productojson.getPrecio_compra());
			}
			if (productojson.getPrecio_venta() > 0) {
				producto.setPrecio_venta(productojson.getPrecio_venta());
			}
			if (productojson.getId_proveedor() > 0) {
				producto.setId_proveedor(productojson.getId_proveedor());
			}
			producto.setCreate_date(ObtenerUTC());
			producto.setUpdate_date(ObtenerUTC());
			producto.setEnable(true);
			sessionproducto.save(producto);
			tx.commit();
			return Response.status(200).entity("Operacion exitosa!!").build();
		} catch (RuntimeException e) {
			e.printStackTrace();
			if ((tx != null) && (tx.isActive())) {
				tx.rollback();
			}
			return Response.status(200).entity("Error: " + e).build();
		}
		

	}

	@PUT
	@Path("/update/")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateProducto(Productojackson productojson) {
		Session sessionproducto = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = sessionproducto.beginTransaction();
		
		try {
			ObjectMapper mapper = new ObjectMapper();
			String jsonStr = "";
			Producto producto = (Producto) sessionproducto.get(Producto.class, productojson.getId_producto());
			if (producto != null) {
				if (producto.getEnable()) {
					if (productojson.getNombre() != null) {
						producto.setNombre(productojson.getNombre());
					}
					if (productojson.getDescripcion() != null) {
						producto.setDescripcion(productojson.getDescripcion());
					}
					if (productojson.getPrecio_compra() > 0) {
						producto.setPrecio_compra(productojson.getPrecio_compra());
					}
					if (productojson.getPrecio_venta() > 0) {
						producto.setPrecio_venta(productojson.getPrecio_venta());
					}
					if (productojson.getId_proveedor() > 0) {
						producto.setId_proveedor(productojson.getId_proveedor());
					}
					producto.setUpdate_date(ObtenerUTC());
					producto.setEnable(true);
					//contenido = producto.toString();
					tx.commit();
					jsonStr += mapper.writerWithDefaultPrettyPrinter().writeValueAsString(producto);
					return Response.status(200).entity(jsonStr).build();
				} else {
					return Response.status(200).entity("No se puedo efectuar la operacion ").build();
				}
			} else {
				return Response.status(200).entity("No se encontro producto con los datos ingresados ").build();
			}
		} catch (RuntimeException e) {
			e.printStackTrace();
			if ((tx != null) && (tx.isActive())) {
				tx.rollback();
			}
			return Response.status(200).entity("Error: " + e).build();
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return Response.status(200).entity("No se puedo efectuar la operacion ").build();
	}

	@GET
	@Path("/search/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response searchProducto(@PathParam("id") long id) {
		Session sessionproducto = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = sessionproducto.beginTransaction();
		try {
			ObjectMapper mapper = new ObjectMapper();
			String jsonStr = "";
			Producto producto = (Producto) sessionproducto.get(Producto.class, id);
			if (producto != null) {

				if (producto.getEnable()) {
					jsonStr += mapper.writerWithDefaultPrettyPrinter().writeValueAsString(producto);
					return Response.status(200).entity(jsonStr).build();
				} else {
					return Response.status(200).entity("No se puede efectuar esta operacion").build();
				}
			}else {
				return Response.status(200).entity("Error ....").build();
			}
		} catch (RuntimeException e) {
			e.printStackTrace();
			if ((tx != null) && (tx.isActive())) {
				tx.rollback();
			}
			return Response.status(200).entity("Error: " + e).build();
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return Response.status(200).entity("No se puede efectuar esta operacion").build();
	}

	@GET
	@Path("/search")
	@Produces(MediaType.APPLICATION_JSON)
	public Response searchProductoTodo() {
		Session sessionproducto = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = sessionproducto.beginTransaction();
		try {
			ObjectMapper mapper = new ObjectMapper();
			String jsonStr = "";
			@SuppressWarnings("unchecked")
			List<Producto> productos = (List<Producto>) sessionproducto.createCriteria(Producto.class)
				.add(Restrictions.eq("enable",true)).list();
			if (productos != null) {
				for (Iterator iterator = productos.iterator(); iterator.hasNext();) {
					Producto producto2 = (Producto) iterator.next();
					jsonStr += mapper.writerWithDefaultPrettyPrinter().writeValueAsString(producto2);
				}
				return Response.status(200).entity(jsonStr).build();
			}else {
				return Response.status(200).entity("Error ....").build();
			}
		} catch (RuntimeException e) {
			e.printStackTrace();
			if ((tx != null) && (tx.isActive())) {
				tx.rollback();
			}
			return Response.status(200).entity("Error: " + e).build();
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return Response.status(200).entity("No se puede efectuar esta operacion").build();
	}
	
	@GET
	@Path("/search/proveedor/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response searchProductoProveedor(@PathParam("id") long id) {
		Session sessionproducto = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = sessionproducto.beginTransaction();
		try {
			ObjectMapper mapper = new ObjectMapper();
			String jsonStr = "";
			@SuppressWarnings("unchecked")
			List<Producto> producto = (List<Producto>) sessionproducto.createCriteria(Producto.class)
				.add(Restrictions.eq("Id_proveedor",id))
				.add(Restrictions.eq("enable",true)).list();
			//List<Producto> listaproductor = new ArrayList<>();
			if (producto != null) {
				for (Iterator iterator = producto.iterator(); iterator.hasNext();) {
					Producto producto2 = (Producto) iterator.next();
					jsonStr += mapper.writerWithDefaultPrettyPrinter().writeValueAsString(producto2);
					
				}
				
				return Response.status(200).entity(jsonStr).build();
			}else {
				return Response.status(200).entity("Error ....").build();
			}
		} catch (RuntimeException e) {
			e.printStackTrace();
			if ((tx != null) && (tx.isActive())) {
				tx.rollback();
			}
			return Response.status(200).entity("Error: " + e).build();
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return Response.status(200).entity("No se puede efectuar esta operacion").build();
	}
	
	@DELETE
	@Path("/delete/{id}")
	public Response deleteProducto(@PathParam("id") long id) {
		Session sessionproducto = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = sessionproducto.beginTransaction();

		try {
			Producto producto = (Producto) sessionproducto.get(Producto.class, id);
			if (producto != null) {
				if (producto.getEnable()) {
					producto.setEnable(false);
					producto.setUpdate_date(ObtenerUTC());
					tx.commit();
					return Response.status(200).entity("Se elimino correctamente").build();
				} else {
					return Response.status(200).entity("No se encuentra el producto").build();
				}
			} else {
				return Response.status(200).entity("Error no se puedo realizar esta operacion").build();
			}
		} catch (RuntimeException e) {
			e.printStackTrace();
			return Response.status(200).entity("Error: " + e).build();
		}
		// return Response.status(200).entity("Error... ").build();

	}
	
	public static String ObtenerUTC() {
		final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
		final String utcTime = sdf.format(new Date());

		return utcTime;
	}

	
}
