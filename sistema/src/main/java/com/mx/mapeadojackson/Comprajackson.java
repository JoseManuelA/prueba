package com.mx.mapeadojackson;


public class Comprajackson {

	private long Id_compra;
	private long Id_proveedor;
	private long Id_producto;
	private long Id_suc;
	private long cantidad;
	private long precio_compra;
	private long precio_venta;
	private String fecha;
	private String create_date;
	private String update_date;
	private boolean enable;

	public Comprajackson() {
		// TODO Auto-generated constructor stub
	}

	public long getId_compra() {
		return Id_compra;
	}

	public void setId_compra(long id_compra) {
		Id_compra = id_compra;
	}

	public long getId_proveedor() {
		return Id_proveedor;
	}

	public void setId_proveedor(long id_proveedor) {
		Id_proveedor = id_proveedor;
	}

	public long getId_producto() {
		return Id_producto;
	}

	public void setId_producto(long id_producto) {
		Id_producto = id_producto;
	}

	public long getId_suc() {
		return Id_suc;
	}

	public void setId_suc(long id_suc) {
		Id_suc = id_suc;
	}

	public long getCantidad() {
		return cantidad;
	}

	public void setCantidad(long cantidad) {
		this.cantidad = cantidad;
	}

	public long getPrecio_compra() {
		return precio_compra;
	}

	public void setPrecio_compra(long precio_compra) {
		this.precio_compra = precio_compra;
	}

	public long getPrecio_venta() {
		return precio_venta;
	}

	public void setPrecio_venta(long precio_venta) {
		this.precio_venta = precio_venta;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getCreate_date() {
		return create_date;
	}

	public void setCreate_date(String create_date) {
		this.create_date = create_date;
	}

	public String getUpdate_date() {
		return update_date;
	}

	public void setUpdate_date(String update_date) {
		this.update_date = update_date;
	}

	public boolean isEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	
}
