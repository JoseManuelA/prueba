package com.mx.entidades;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Tipo_trabajador")
public class Tipo_trabajador {
	
	@Id
	@Column(name="Id_tipo")
	private long Id_tipo;
	
	@Column(name="puesto")
	private String puesto;
	
	@Column(name="create_date")
	private String create_date;
	
	@Column(name="update_date")
	private String update_date;
	
	@Column(name="enable")
	private boolean enable;

	public Tipo_trabajador() {
		// TODO Auto-generated constructor stub
	}

	public long getId_tipo() {
		return Id_tipo;
	}

	public void setId_tipo(long id_tipo) {
		Id_tipo = id_tipo;
	}

	public String getPuesto() {
		return puesto;
	}

	public void setPuesto(String puesto) {
		this.puesto = puesto;
	}

	public String getCreate_date() {
		return create_date;
	}

	public void setCreate_date(String create_date) {
		this.create_date = create_date;
	}

	public String getUpdate_date() {
		return update_date;
	}

	public void setUpdate_date(String update_date) {
		this.update_date = update_date;
	}

	public boolean isEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	@Override
	public String toString() {
		return "Tipo_trabajador [Id_tipo=" + Id_tipo + ", puesto=" + puesto + ", create_date=" + create_date
				+ ", update_date=" + update_date + ", enable=" + enable + "]";
	}
	
	

}
