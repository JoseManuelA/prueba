package com.pruebawebapp.entidadesjson;

public class Direccion {
	
	private int Id;
	
	private String colonia;
	
	private String calle;

	private int Id_usr;
	

	public Direccion() {
		// TODO Auto-generated constructor stub
	}


	public int getId() {
		return Id;
	}


	public void setId(int id) {
		Id = id;
	}


	public String getColonia() {
		return colonia;
	}


	public void setColonia(String colonia) {
		this.colonia = colonia;
	}


	public String getCalle() {
		return calle;
	}


	public void setCalle(String calle) {
		this.calle = calle;
	}


	public int getId_usr() {
		return Id_usr;
	}


	public void setId_usr(int id_usr) {
		Id_usr = id_usr;
	}


	@Override
	public String toString() {
		return "Direccion [Id=" + Id + ", colonia=" + colonia + ", calle=" + calle + ", Id_usr=" + Id_usr + "]";
	}
	
	

}
