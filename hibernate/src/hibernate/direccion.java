package hibernate;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "direccion")
public class direccion implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "Id")
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	private int Id;
	
	@Column(name = "colonia")
	private String colonia;
	
	@Column(name = "calle")
	private String calle;

	@Column(name = "Id_usr")
	private String Id_usr;

	public direccion() {
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public String getColonia() {
		return colonia;
	}

	public void setColonia(String colonia) {
		this.colonia = colonia;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public String getId_usr() {
		return Id_usr;
	}

	public void setId_usr(String id_usr) {
		Id_usr = id_usr;
	}

	@ManyToOne
	@JoinColumn(name="Id_usr")
	private Usuarios usuario;

	public Usuarios getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuarios usuario) {
		this.usuario = usuario;
	}
	
	

}
