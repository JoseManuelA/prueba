package com.mx.mapeadojackson;

import java.util.List;

import com.mx.entidades.Transferencias;

public class Pruebatransferencia {
	
	private long origen;
	private long transferenciaR;
	private long transferenciaE;
	private List<Transferencias> transferencias;


	public long getOrigen() {
		return origen;
	}



	public void setOrigen(long origen) {
		this.origen = origen;
	}



	public long getTransferenciaR() {
		return transferenciaR;
	}



	public void setTransferenciaR(long transferenciaR) {
		this.transferenciaR = transferenciaR;
	}



	public long getTransferenciaE() {
		return transferenciaE;
	}



	public void setTransferenciaE(long transferenciaE) {
		this.transferenciaE = transferenciaE;
	}


	public List<Transferencias> getTransferencias() {
		return transferencias;
	}



	public void setTransferencias(List<Transferencias> transferencias) {
		this.transferencias = transferencias;
	}



	public Pruebatransferencia() {
		// TODO Auto-generated constructor stub
	}

}
