-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 27, 2019 at 07:13 PM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `almacen`
--

-- --------------------------------------------------------

--
-- Table structure for table `Almacen`
--

CREATE TABLE `Almacen` (
  `Id_almacen` int(11) NOT NULL,
  `Id_suc` int(11) NOT NULL,
  `Id_producto` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `enable` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Almacen`
--

INSERT INTO `Almacen` (`Id_almacen`, `Id_suc`, `Id_producto`, `cantidad`, `create_date`, `update_date`, `enable`) VALUES
(1, 4, 8, 5, '2019-02-25 22:05:27', '2019-02-27 22:46:21', 1),
(2, 4, 7, 10, '2019-02-25 22:05:27', '2019-02-27 22:47:55', 1),
(3, 3, 8, 7, '2019-02-26 03:17:50', '2019-02-26 23:51:19', 1),
(4, 3, 7, 10, '2019-02-26 03:22:48', '2019-02-26 03:26:02', 1),
(5, 4, 4, 20, '2019-02-26 05:06:15', '2019-02-27 23:44:37', 1),
(6, 4, 5, 25, '2019-02-26 05:06:15', '2019-02-27 23:42:56', 1),
(7, 3, 4, 15, '2019-02-26 05:22:32', '2019-02-26 05:22:32', 1),
(8, 4, 9, 53, '2019-02-27 22:05:03', '2019-02-27 22:33:03', 1),
(9, 2, 4, 18, '2019-02-27 23:42:56', '2019-02-27 23:44:37', 1),
(10, 2, 5, 25, '2019-02-27 23:42:56', '2019-02-27 23:42:56', 1);

-- --------------------------------------------------------

--
-- Table structure for table `Compras`
--

CREATE TABLE `Compras` (
  `Id_compra` int(11) NOT NULL,
  `Id_proveedor` int(11) NOT NULL,
  `Id_producto` int(11) NOT NULL,
  `Id_suc` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio_compra` int(11) NOT NULL,
  `precio_venta` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `enable` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Compras`
--

INSERT INTO `Compras` (`Id_compra`, `Id_proveedor`, `Id_producto`, `Id_suc`, `cantidad`, `precio_compra`, `precio_venta`, `fecha`, `create_date`, `update_date`, `enable`) VALUES
(1, 8, 1, 4, 10, 15, 25, '2019-02-25 15:51:58', '2019-02-25 21:51:58', '2019-02-25 21:51:58', 1),
(2, 1, 8, 4, 10, 15, 25, '2019-02-25 16:00:48', '2019-02-25 22:00:48', '2019-02-25 22:00:48', 1),
(3, 1, 8, 4, 10, 15, 25, '2019-02-25 16:02:12', '2019-02-25 22:02:12', '2019-02-25 22:02:12', 1),
(4, 1, 6, 4, 10, 10, 20, '2019-02-25 16:02:12', '2019-02-25 22:02:12', '2019-02-25 22:02:12', 1),
(5, 1, 8, 4, 5, 10, 15, '2019-02-25 16:05:27', '2019-02-25 22:05:27', '2019-02-25 22:05:27', 1),
(6, 1, 7, 4, 5, 10, 15, '2019-02-25 16:05:27', '2019-02-25 22:05:27', '2019-02-25 22:05:27', 1),
(7, 1, 8, 4, 5, 10, 15, '2019-02-25 16:17:09', '2019-02-25 22:17:09', '2019-02-25 22:17:09', 1),
(8, 1, 7, 4, 5, 10, 15, '2019-02-25 16:17:09', '2019-02-25 22:17:09', '2019-02-25 22:17:09', 1),
(10, 1, 8, 4, 5, 10, 15, '2019-02-25 16:32:22', '2019-02-25 22:32:22', '2019-02-25 22:32:22', 1),
(11, 1, 8, 4, 5, 10, 15, '2019-02-25 16:35:47', '2019-02-25 22:35:47', '2019-02-25 22:35:47', 1),
(12, 1, 8, 4, 5, 10, 15, '2019-02-25 16:36:38', '2019-02-25 22:36:38', '2019-02-25 22:36:38', 1),
(13, 1, 8, 4, 5, 10, 15, '2019-02-25 16:39:36', '2019-02-25 22:39:36', '2019-02-25 22:39:36', 1),
(14, 1, 7, 4, 5, 10, 15, '2019-02-25 16:54:06', '2019-02-25 22:54:06', '2019-02-25 22:54:06', 1),
(15, 1, 8, 4, 5, 10, 15, '2019-02-25 16:55:34', '2019-02-25 22:55:34', '2019-02-25 22:55:34', 1),
(16, 1, 7, 4, 5, 10, 15, '2019-02-25 16:55:34', '2019-02-25 22:55:34', '2019-02-25 22:55:34', 1),
(17, 1, 8, 4, 5, 10, 15, '2019-02-25 16:55:58', '2019-02-25 22:55:58', '2019-02-25 22:55:58', 1),
(18, 1, 7, 4, 5, 10, 15, '2019-02-25 16:55:58', '2019-02-25 22:55:58', '2019-02-25 22:55:58', 1),
(19, 1, 8, 4, 5, 10, 15, '2019-02-25 16:56:08', '2019-02-25 22:56:08', '2019-02-25 22:56:08', 1),
(20, 1, 7, 4, 5, 10, 15, '2019-02-25 16:56:08', '2019-02-25 22:56:08', '2019-02-25 22:56:08', 1),
(21, 1, 8, 4, 5, 10, 15, '2019-02-25 17:05:19', '2019-02-25 23:05:19', '2019-02-25 23:18:37', 0),
(22, 1, 7, 4, 5, 10, 15, '2019-02-25 17:05:19', '2019-02-25 23:05:19', '2019-02-25 23:05:19', 1),
(23, 1, 8, 4, 5, 10, 15, '2019-02-25 19:26:59', '2019-02-26 01:26:59', '2019-02-26 01:26:59', 1),
(24, 1, 7, 4, 5, 10, 15, '2019-02-25 19:26:59', '2019-02-26 01:26:59', '2019-02-26 01:26:59', 1),
(25, 1, 4, 4, 53, 10, 15, '2019-02-25 23:06:15', '2019-02-26 05:06:15', '2019-02-26 05:09:48', 0),
(26, 1, 5, 4, 25, 10, 15, '2019-02-25 23:06:15', '2019-02-26 05:06:15', '2019-02-26 05:06:15', 1),
(27, 1, 4, 4, 53, 10, 15, '2019-02-25 23:17:01', '2019-02-26 05:17:01', '2019-02-26 05:17:01', 1),
(28, 1, 9, 4, 40, 50, 70, '2019-02-27 16:05:03', '2019-02-27 22:05:03', '2019-02-27 22:16:35', 0),
(29, 1, 9, 4, 53, 10, 15, '2019-02-27 16:33:03', '2019-02-27 22:33:03', '2019-02-27 22:33:03', 1),
(30, 1, 5, 4, 25, 10, 15, '2019-02-27 16:33:03', '2019-02-27 22:33:03', '2019-02-27 22:33:03', 1);

-- --------------------------------------------------------

--
-- Table structure for table `Productos`
--

CREATE TABLE `Productos` (
  `Id_producto` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `descripcion` text NOT NULL,
  `precio_compra` int(11) NOT NULL,
  `precio_venta` int(11) NOT NULL,
  `Id_proveedor` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `enable` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Productos`
--

INSERT INTO `Productos` (`Id_producto`, `nombre`, `descripcion`, `precio_compra`, `precio_venta`, `Id_proveedor`, `create_date`, `update_date`, `enable`) VALUES
(1, 'Producto 1', 'Descripcion producto1', 15, 20, 1, '2019-02-22 17:34:47', '2019-02-25 23:00:41', 1),
(2, 'Producto 2', 'Descripcion producto 2', 15, 20, 1, '2019-02-22 17:36:48', '2019-02-25 22:59:52', 1),
(3, 'Producto 3', 'Descripcion producto 4', 15, 20, 1, '2019-02-22 17:38:05', '2019-02-25 23:00:00', 1),
(4, 'Producto 4 nuevo', 'Descripcion prodcto 4 nuevo', 17, 22, 1, '2019-02-22 17:39:52', '2019-02-27 21:56:49', 1),
(5, 'Producto 5', 'Descripcion producto 5', 15, 20, 1, '2019-02-22 17:45:07', '2019-02-25 23:00:27', 1),
(6, 'Producto 6', 'Descripcion producto 6', 15, 20, 1, '2019-02-22 17:47:40', '2019-02-25 23:00:33', 1),
(7, 'Producto nuevo 7', 'Descripcion producto  nuevo 7', 5, 50, 1, '2019-02-22 17:48:23', '2019-02-22 17:48:23', 1),
(8, 'Producto nuevo 7', 'Descripcion producto  nuevo 7', 5, 50, 1, '2019-02-23 04:30:57', '2019-02-25 15:37:29', 1),
(9, 'Producto XZ', 'Descripcion XZ', 15, 20, 1, '2019-02-27 21:54:34', '2019-02-27 15:55:28', 1);

-- --------------------------------------------------------

--
-- Table structure for table `Proveedores`
--

CREATE TABLE `Proveedores` (
  `Id_proveedor` int(11) NOT NULL,
  `nombre` varchar(60) NOT NULL,
  `telefono` varchar(15) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `enable` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Proveedores`
--

INSERT INTO `Proveedores` (`Id_proveedor`, `nombre`, `telefono`, `create_date`, `update_date`, `enable`) VALUES
(1, 'proveedor renovada S.A de C.V', '555555555', '2019-02-22 19:23:17', '2019-02-23 01:55:02', 1),
(2, 'proveedor 2 S.A de C.V', '34582469', '2019-02-22 19:37:42', '2019-02-22 19:37:42', 1),
(3, 'proveedor renovada S.A de C.V', '34582469', '2019-02-23 04:33:51', '2019-02-23 04:35:19', 0),
(4, 'Nuevo Proveedor S.A de C.V', '01800551056', '2019-02-27 21:46:36', '2019-02-27 15:48:43', 1);

-- --------------------------------------------------------

--
-- Table structure for table `Tienda`
--

CREATE TABLE `Tienda` (
  `Id_suc` int(11) NOT NULL,
  `nombre` varchar(60) NOT NULL,
  `colonia` text NOT NULL,
  `calle` text NOT NULL,
  `cp` varchar(15) NOT NULL,
  `numero` varchar(10) NOT NULL,
  `telefono` text NOT NULL,
  `matriz` int(1) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `enable` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Tienda`
--

INSERT INTO `Tienda` (`Id_suc`, `nombre`, `colonia`, `calle`, `cp`, `numero`, `telefono`, `matriz`, `create_date`, `update_date`, `enable`) VALUES
(2, 'La tiendita de la esquina', 'ventura puente', 'puente chico', '5210', '3333333', '1111111', 1, '2019-02-21 19:15:27', '2019-02-27 21:36:34', 1),
(3, 'latiendita', 'villas', 'callevilla', '58210', '141A', '3133149', 1, '2019-02-21 18:25:51', '2019-02-21 18:25:51', 1),
(4, 'la tiendita Central', 'villas del pedregal', 'callevilla', '58210', '125A', '3133157', 2, '2019-02-21 19:32:55', '2019-02-25 15:38:03', 1),
(5, 'latiendita nueva av solidaridad', 'villas', 'callevilla', '58210', '141A', '3133149', 1, '2019-02-22 16:26:35', '2019-02-22 16:26:35', 1),
(6, 'Abarrotes Azteca', 'El plan', 'Calle uno', '52100', '98954565', '2222222', 1, '2019-02-27 21:38:30', '2019-02-27 15:41:04', 1);

-- --------------------------------------------------------

--
-- Table structure for table `Tipo_trabajador`
--

CREATE TABLE `Tipo_trabajador` (
  `Id_tipo` int(11) NOT NULL,
  `puesto` varchar(50) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `enable` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Tipo_trabajador`
--

INSERT INTO `Tipo_trabajador` (`Id_tipo`, `puesto`, `create_date`, `update_date`, `enable`) VALUES
(1, 'Gerente', '2019-02-22 21:08:05', '2019-02-20 19:15:27', 1),
(2, 'Vendedor', '2019-02-22 21:08:43', '2019-02-20 19:19:08', 1),
(3, 'Almacen', '2019-02-22 21:09:02', '2019-02-20 19:27:15', 1);

-- --------------------------------------------------------

--
-- Table structure for table `Trabajadores`
--

CREATE TABLE `Trabajadores` (
  `Id_trabajador` int(11) NOT NULL,
  `Id_suc` int(11) NOT NULL,
  `nombre_trabajador` varchar(50) NOT NULL,
  `apellidop_trabajador` varchar(90) NOT NULL,
  `apellidom_trabajador` varchar(50) NOT NULL,
  `Id_tipo` int(11) NOT NULL,
  `password` text NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `enable` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Trabajadores`
--

INSERT INTO `Trabajadores` (`Id_trabajador`, `Id_suc`, `nombre_trabajador`, `apellidop_trabajador`, `apellidom_trabajador`, `Id_tipo`, `password`, `create_date`, `update_date`, `enable`) VALUES
(1, 3, 'Jose manuel', 'ALvarez', 'Bucio', 3, '3336558', '2019-02-21 23:21:18', '2019-02-26 17:49:36', 0),
(2, 3, 'Jose Manuel', 'Alvarez', 'Bucio', 2, '3336558', '2019-02-21 20:10:32', '2019-02-27 21:27:35', 1),
(3, 4, 'Juan Antonio', 'Hernandez', 'Guitierrez', 3, '12325485', '2019-02-23 02:28:54', '2019-02-25 20:47:28', 1),
(4, 4, 'Antonio', 'Sanchez', 'Guitierrez', 2, '12325485', '2019-02-23 04:07:59', '2019-02-25 19:45:40', 1),
(5, 2, 'Gilberto', 'Saul', 'Garibay', 1, '12325485', '2019-02-23 04:09:33', '2019-02-23 04:09:33', 1),
(6, 3, 'Fernando', 'Gilberto', 'Cruz', 3, '3336558', '2019-02-27 21:29:16', '2019-02-27 21:29:16', 1);

-- --------------------------------------------------------

--
-- Table structure for table `Transferencias`
--

CREATE TABLE `Transferencias` (
  `Id_transferencia` int(11) NOT NULL,
  `origen` int(11) NOT NULL,
  `destino` int(11) NOT NULL,
  `Id_trabajador` int(11) NOT NULL,
  `Id_producto` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `enable` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Transferencias`
--

INSERT INTO `Transferencias` (`Id_transferencia`, `origen`, `destino`, `Id_trabajador`, `Id_producto`, `cantidad`, `fecha`, `create_date`, `update_date`, `enable`) VALUES
(3, 4, 3, 3, 8, 5, '2019-02-25 21:17:50', '2019-02-26 03:17:50', '2019-02-26 03:17:50', 1),
(4, 4, 3, 3, 8, 5, '2019-02-25 21:19:54', '2019-02-26 03:19:54', '2019-02-26 03:19:54', 1),
(6, 4, 3, 3, 7, 5, '2019-02-25 21:22:48', '2019-02-26 03:22:48', '2019-02-26 03:22:48', 1),
(8, 4, 3, 3, 8, 5, '2019-02-25 21:25:42', '2019-02-26 03:25:42', '2019-02-26 03:25:42', 1),
(9, 4, 3, 3, 7, 5, '2019-02-25 21:25:42', '2019-02-26 03:25:42', '2019-02-26 03:25:42', 1),
(10, 4, 3, 3, 8, 5, '2019-02-25 21:26:02', '2019-02-26 03:26:02', '2019-02-26 03:26:02', 0),
(11, 4, 3, 3, 7, 5, '2019-02-25 21:26:02', '2019-02-26 03:26:02', '2019-02-26 03:26:02', 0),
(12, 4, 3, 3, 4, 15, '2019-02-25 23:22:32', '2019-02-26 05:22:32', '2019-02-26 05:22:32', 1),
(13, 3, 4, 1, 8, 5, '2019-02-26 17:51:19', '2019-02-26 23:51:19', '2019-02-26 23:51:19', 1),
(14, 4, 2, 3, 4, 18, '2019-02-27 17:42:56', '2019-02-27 23:42:56', '2019-02-27 23:42:56', 1),
(15, 4, 2, 3, 5, 25, '2019-02-27 17:42:56', '2019-02-27 23:42:56', '2019-02-27 23:42:56', 1),
(16, 4, 2, 3, 4, 10, '2019-02-27 17:44:37', '2019-02-27 23:44:37', '2019-02-27 23:44:37', 0);

-- --------------------------------------------------------

--
-- Table structure for table `Ventas`
--

CREATE TABLE `Ventas` (
  `Id_venta` int(11) NOT NULL,
  `Id_suc` int(11) NOT NULL,
  `Id_trabajador` int(11) NOT NULL,
  `Id_producto` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `enable` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Ventas`
--

INSERT INTO `Ventas` (`Id_venta`, `Id_suc`, `Id_trabajador`, `Id_producto`, `cantidad`, `total`, `fecha`, `create_date`, `update_date`, `enable`) VALUES
(1, 4, 4, 0, 5, 20, '2019-02-22 13:20:30', '2019-02-26 23:40:10', '2019-02-26 00:19:39', 1),
(2, 4, 4, 0, 5, 20, '2019-02-22 13:20:30', '2019-02-26 23:41:00', '2019-02-26 00:19:39', 1),
(3, 2, 4, 0, 5, 20, '2019-02-25 13:35:30', '2019-02-26 23:43:42', '2019-02-26 00:44:49', 1),
(4, 2, 4, 0, 5, 20, '2019-02-25 13:35:30', '2019-02-26 23:43:48', '2019-02-26 00:44:49', 1),
(5, 2, 4, 7, 5, 20, '2019-02-25 13:40:30', '2019-02-26 23:44:43', '2019-02-27 15:11:04', 1),
(6, 2, 4, 8, 5, 20, '2019-02-25 16:35:43', '2019-02-26 23:45:29', '2019-02-26 00:54:21', 1),
(7, 2, 4, 7, 5, 20, '2019-02-25 13:53:30', '2019-02-26 23:44:29', '2019-02-26 01:22:32', 1),
(8, 2, 4, 8, 5, 20, '2019-02-25 13:59:30', '2019-02-26 23:44:55', '2019-02-26 01:22:32', 1),
(9, 2, 4, 7, 5, 20, '2019-02-25 13:53:30', '2019-02-26 23:44:23', '2019-02-26 01:24:26', 1),
(10, 2, 4, 8, 5, 20, '2019-02-25 17:36:30', '2019-02-26 23:46:06', '2019-02-26 01:24:26', 1),
(11, 2, 4, 8, 5, 20, '2019-02-25 17:36:30', '2019-02-26 23:46:01', '2019-02-26 01:25:49', 1),
(12, 2, 4, 8, 5, 20, '2019-02-25 17:20:30', '2019-02-26 23:45:37', '2019-02-26 01:28:48', 1),
(13, 4, 4, 7, 5, 20, '2019-02-25 13:43:30', '2019-02-26 23:44:11', '2019-02-26 01:46:31', 1),
(14, 4, 4, 8, 5, 20, '2019-02-25 16:20:43', '2019-02-26 23:42:41', '2019-02-26 01:46:31', 1),
(15, 4, 4, 7, 10, 20, '2019-02-25 13:43:30', '2019-02-26 23:44:01', '2019-02-26 01:47:12', 1),
(16, 4, 4, 8, 10, 20, '2019-02-25 16:20:43', '2019-02-26 23:45:43', '2019-02-26 01:47:12', 1),
(17, 3, 2, 8, 3, 20, '2019-02-25 18:36:30', '2019-02-26 23:46:14', '2019-02-26 05:39:41', 1),
(18, 4, 4, 8, 10, 0, '2019-02-27 16:44:06', '2019-02-27 22:44:06', '2019-02-27 22:44:06', 1),
(19, 4, 4, 8, 5, 0, '2019-02-27 16:46:21', '2019-02-27 22:46:21', '2019-02-27 22:46:21', 1),
(20, 4, 4, 7, 2, 0, '2019-02-27 16:46:21', '2019-02-27 22:46:21', '2019-02-27 22:47:55', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Almacen`
--
ALTER TABLE `Almacen`
  ADD PRIMARY KEY (`Id_almacen`),
  ADD KEY `unionsuc` (`Id_suc`);

--
-- Indexes for table `Compras`
--
ALTER TABLE `Compras`
  ADD PRIMARY KEY (`Id_compra`);

--
-- Indexes for table `Productos`
--
ALTER TABLE `Productos`
  ADD PRIMARY KEY (`Id_producto`);

--
-- Indexes for table `Proveedores`
--
ALTER TABLE `Proveedores`
  ADD PRIMARY KEY (`Id_proveedor`);

--
-- Indexes for table `Tienda`
--
ALTER TABLE `Tienda`
  ADD PRIMARY KEY (`Id_suc`);

--
-- Indexes for table `Tipo_trabajador`
--
ALTER TABLE `Tipo_trabajador`
  ADD PRIMARY KEY (`Id_tipo`);

--
-- Indexes for table `Trabajadores`
--
ALTER TABLE `Trabajadores`
  ADD PRIMARY KEY (`Id_trabajador`),
  ADD KEY `tipotrabajador` (`Id_tipo`),
  ADD KEY `tiendarelacion` (`Id_suc`);

--
-- Indexes for table `Transferencias`
--
ALTER TABLE `Transferencias`
  ADD PRIMARY KEY (`Id_transferencia`),
  ADD KEY `uniontienda` (`origen`),
  ADD KEY `uniontiendadestino` (`destino`),
  ADD KEY `unionproductos` (`Id_producto`);

--
-- Indexes for table `Ventas`
--
ALTER TABLE `Ventas`
  ADD PRIMARY KEY (`Id_venta`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Almacen`
--
ALTER TABLE `Almacen`
  MODIFY `Id_almacen` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `Compras`
--
ALTER TABLE `Compras`
  MODIFY `Id_compra` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `Productos`
--
ALTER TABLE `Productos`
  MODIFY `Id_producto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `Proveedores`
--
ALTER TABLE `Proveedores`
  MODIFY `Id_proveedor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `Tienda`
--
ALTER TABLE `Tienda`
  MODIFY `Id_suc` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `Tipo_trabajador`
--
ALTER TABLE `Tipo_trabajador`
  MODIFY `Id_tipo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `Trabajadores`
--
ALTER TABLE `Trabajadores`
  MODIFY `Id_trabajador` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `Transferencias`
--
ALTER TABLE `Transferencias`
  MODIFY `Id_transferencia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `Ventas`
--
ALTER TABLE `Ventas`
  MODIFY `Id_venta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
