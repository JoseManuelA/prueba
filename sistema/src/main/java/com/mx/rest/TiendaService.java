package com.mx.rest;

import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;import org.apache.http.impl.entity.DisallowIdentityContentLengthStrategy;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mx.configuraciones.HibernateUtil;
import com.mx.entidades.Almacen;
import com.mx.entidades.Compras;
import com.mx.entidades.Tienda;
import com.mx.entidades.Trabajadores;
import com.mx.funciones.Funciones;
import com.mx.mapeadojackson.Tiendajackson;
import com.mx.mapeadojackson.Trabajadoresjackson;

@Path("/Tienda")
public class TiendaService implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TiendaService() {
		// TODO Auto-generated constructor stub
	}
	

	@POST
	@Path("/new/")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addTienda(Tiendajackson tiendajson) {
		Funciones funcion = new Funciones();
		Session sessiontienda = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = sessiontienda.beginTransaction();
		try {
			Tienda tienda = new Tienda();
			if (tiendajson.getNombre()!= null) {
				tienda.setNombre(tiendajson.getNombre());
			}
			if (tiendajson.getCalle() != null) {
				tienda.setCalle(tiendajson.getCalle());
			}
			if (tiendajson.getColonia() != null) {
				tienda.setColonia(tiendajson.getColonia());
			}
			if (tiendajson.getNumero()!=null) {
				tienda.setNumero(tiendajson.getNumero() );
			}
			if (tiendajson.getCp() !=null) {
				tienda.setCp(tiendajson.getCp() );
			}
			if (tiendajson.getTelefono() !=null) {
				tienda.setTelefono(tiendajson.getTelefono() );
			}
			if (tiendajson.getMatriz() >0 ) {
				tienda.setMatriz(tiendajson.getMatriz() );
			}
			tienda.setCreate_date(funcion.getUTC());
			tienda.setUpdate_date(funcion.getUTC());
			tienda.setEnable(true);
			sessiontienda.save(tienda);
			tx.commit();
			return Response.status(200).entity("Oepracion exitosa!!").build();
		} catch (RuntimeException e) {
			// TODO: handle exception
			e.printStackTrace();
			return Response.status(200).entity("Error: " + e).build();
		}

	}
	
	@PUT
	@Path("/update/")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateTienda(Tiendajackson tiendajson) {
		Funciones funcion = new Funciones();
		Session sessiontienda = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = sessiontienda.beginTransaction();
		try {
			ObjectMapper mapper = new ObjectMapper();
			String jsonStr = "";
			Tienda tienda = (Tienda) sessiontienda.get(Tienda.class, tiendajson.getId_suc());
			if (tienda != null) {
				if (tienda.isEnable()) {
					if (tiendajson.getNombre()!= null) {
						tienda.setNombre(tiendajson.getNombre());
					}
					if (tiendajson.getCalle() != null) {
						tienda.setCalle(tiendajson.getCalle());
					}
					if (tiendajson.getColonia() != null) {
						tienda.setColonia(tiendajson.getColonia());
					}
					if (tiendajson.getNumero()!=null) {
						tienda.setNumero(tiendajson.getNumero() );
					}
					if (tiendajson.getCp() !=null) {
						tienda.setCp(tiendajson.getCp() );
					}
					if (tiendajson.getTelefono() !=null) {
						tienda.setTelefono(tiendajson.getTelefono() );
					}
					if (tiendajson.getMatriz() >0 ) {
						tienda.setMatriz(tiendajson.getMatriz() );
					}
					tienda.setUpdate_date(funcion.getUTC());
					tienda.setEnable(true);
					jsonStr += mapper.writerWithDefaultPrettyPrinter().writeValueAsString(tienda);
					//contenido = tienda.toString();
					tx.commit();
					return Response.status(200).entity(jsonStr).build();
				} else {
					return Response.status(200).entity("No se puedo efectuar la operacion ").build();
				}
			} else {
				return Response.status(200).entity("No se encontro Tienda con los datos ingresados ").build();
			}
		} catch (RuntimeException e) {
			e.printStackTrace();
			if ((tx != null) && (tx.isActive())) {
				tx.rollback();
			}
			return Response.status(200).entity("Error: " + e).build();
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return Response.status(200).entity("No se puedo efectuar la operacion ").build();
	}
	
	@GET
	@Path("/search/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response searchTienda(@PathParam("id") long id) {
		Session sessiontienda = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = sessiontienda.beginTransaction();
		try {
			ObjectMapper mapper = new ObjectMapper();
			String jsonStr = "";
			Tienda tienda = (Tienda) sessiontienda.get(Tienda.class, id);
			if (tienda != null) {

				if (tienda.isEnable()) {
					jsonStr += mapper.writerWithDefaultPrettyPrinter().writeValueAsString(tienda);
					return Response.status(200).entity(jsonStr).build();
				} else {
					return Response.status(200).entity("No se puede efectuar esta operacion").build();
				}
			}else {
				return Response.status(200).entity("Error ....").build();
			}
		} catch (RuntimeException e) {
			e.printStackTrace();
			if ((tx != null) && (tx.isActive())) {
				tx.rollback();
			}
			return Response.status(200).entity("Error: " + e).build();
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return Response.status(200).entity("No se puede efectuar esta operacion").build();
	}
	
	@GET
	@Path("/search")
	@Produces(MediaType.APPLICATION_JSON)
	public Response searchVentaTotal() {
		Session sessionventa = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = sessionventa.beginTransaction();
		try {
			@SuppressWarnings("unchecked")
			List<Tienda> list = (List<Tienda>) sessionventa
					.createCriteria(Tienda.class).list();
			ObjectMapper mapper = new ObjectMapper();
			String jsonStr = "";
			for (Iterator iterator = list.iterator(); iterator.hasNext();) {
				Tienda tiendas = (Tienda) iterator.next();
				jsonStr += mapper.writerWithDefaultPrettyPrinter().writeValueAsString(tiendas);
			}
			return Response.status(200).entity(jsonStr).build();
		} catch (RuntimeException e) {
			e.printStackTrace();
			if ((tx != null) && (tx.isActive())) {
				tx.rollback();
			}
			return Response.status(200).entity("Error: " + e).build();
		}
		catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return Response.status(200).entity("Error en la operacion").build();
	}

	
	
	@GET
	@Path("/almacen")
	@Produces(MediaType.APPLICATION_JSON)
	public Response mostraralmacen(Tiendajackson tiendajson) {
		Session sessionventa = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = sessionventa.beginTransaction();
		
	
		try {
			@SuppressWarnings("unchecked")
			List<Almacen> listalmacen = sessionventa.createCriteria(Almacen.class)
			.add(Restrictions.eq("Id_suc",tiendajson.getId_suc()))
			.add(Restrictions.eq("enable",true)).list();
			ObjectMapper mapper = new ObjectMapper();
			String jsonStr = "";
			for (Almacen almacen : listalmacen) {
				jsonStr += mapper.writerWithDefaultPrettyPrinter().writeValueAsString(almacen);
			}
			return Response.status(200).entity(jsonStr).build();
		} catch (RuntimeException e) {
			e.printStackTrace();
			if ((tx != null) && (tx.isActive())) {
				tx.rollback();
			}
			return Response.status(200).entity("Error: " + e).build();
		}
		catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return Response.status(200).entity("Error en la operacion").build();
	}

	
	@DELETE
	@Path("/delete/{id}")
	public Response deleteTienda(@PathParam("id") long id) {
		Session sessiontienda = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = sessiontienda.beginTransaction();
		try {
			Tienda tienda = (Tienda) sessiontienda.get(Tienda.class, id);
			if (tienda != null) {
				if (tienda.isEnable()) {
					tienda.setEnable(false);
					tx.commit();
					return Response.status(200).entity("Oepracion exitosa!!").build();
				} else {
					return Response.status(200).entity("No se pudo efectuar la operacion").build();
				}
			} else {
				return Response.status(200).entity("Error...!!").build();
			}
		} catch (RuntimeException e) {
			e.printStackTrace();
			return Response.status(200).entity("Error: " + e).build();
		}
	}
	
	
	

}
