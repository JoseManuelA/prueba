package com.mx.mapeadojackson;


public class Provedorjackson {
	private long Id_proveedor;
	private String nombre;
	private String telefono;
	private String create_date;
	private String update_date;
	private boolean enable;

	public Provedorjackson() {
		// TODO Auto-generated constructor stub
	}

	public long getId_proveedor() {
		return Id_proveedor;
	}

	public void setId_proveedor(long id_proveedor) {
		Id_proveedor = id_proveedor;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getcreate_date() {
		return create_date;
	}

	public void setcreate_date(String create_date) {
		this.create_date = create_date;
	}

	public String getUpdate_date() {
		return update_date;
	}

	public void setUpdate_date(String update_date) {
		this.update_date = update_date;
	}

	public boolean isEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}
	

}
