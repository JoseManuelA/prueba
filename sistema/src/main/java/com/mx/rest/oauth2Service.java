package com.mx.rest;

import java.io.Serializable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.oltu.oauth2.as.issuer.MD5Generator;
import org.apache.oltu.oauth2.as.issuer.OAuthIssuerImpl;
import org.apache.oltu.oauth2.as.request.OAuthTokenRequest;
import org.apache.oltu.oauth2.as.response.OAuthASResponse;
import org.apache.oltu.oauth2.common.message.OAuthResponse;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mx.configuraciones.HibernateUtil;
import com.mx.entidades.Trabajadores;
import com.mx.funciones.Funciones;
import com.mx.mapeadojackson.Autentificacion;

@Path("/Prueba")
public class oauth2Service implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public oauth2Service() {
		// TODO Auto-generated constructor stub
	}

	@POST
	@Path("/new/variado")
	// @Consumes(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response addTienda(@Context HttpServletRequest request) {
		
		Session sessiontrabajador = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = sessiontrabajador.beginTransaction();
		try {
			// Trabajadores trabajador = (Trabajadores)
			// sessiontrabajador.get(Trabajadores.class, trabajadorjson.getId_trabajador());
			String contenido = request.getHeader("autorizacion");
			Autentificacion autentificacion = new ObjectMapper().readValue(contenido, Autentificacion.class);
			Trabajadores trabajador = (Trabajadores) sessiontrabajador.get(Trabajadores.class, Long.parseLong(request.getParameter("client_id")));
			OAuthTokenRequest oauthRequest = new OAuthTokenRequest(request);
			OAuthIssuerImpl issuer = new OAuthIssuerImpl(new MD5Generator());
			
			//OauthRequest a =new OauthRequest();
			String accessToken = issuer.accessToken();
			String refreshToken = issuer.refreshToken();
			//String contenido2 = request.getParameter("client_id");
			Funciones funcion = new Funciones();

			
			if (trabajador.getPassword().equals(autentificacion.getPassword())) {
				
				if ((trabajador.getToken_access()!=null) && (trabajador.getToken_refresh()!=null) && 
						(trabajador.getFecha_access()!=null) && (trabajador.getFecha_refresh()!=null)) {
					if (trabajador.getFecha_access().compareTo(funcion.getGMT())>0) {
						//El token sigue activo no se hace nada
					}else {
						if (trabajador.getFecha_refresh().compareTo(funcion.getGMT())>0) {
							trabajador.setToken_access(accessToken);
							trabajador.setFecha_access("" + funcion.addMinutes(5));
							trabajador.setUpdate_date(funcion.getUTC());
							sessiontrabajador.update(trabajador);
						}else {
							trabajador.setToken_access(accessToken);
							trabajador.setFecha_access("" + funcion.addMinutes(5));
							trabajador.setToken_refresh(refreshToken);
							trabajador.setFecha_refresh("" + funcion.addMinutes(10));
							trabajador.setUpdate_date(funcion.getUTC());
							sessiontrabajador.update(trabajador);
						}
					}
				}else {	
					trabajador.setToken_access(accessToken);
					trabajador.setFecha_access("" + funcion.addMinutes(20));
					trabajador.setToken_refresh(refreshToken);
					trabajador.setFecha_refresh("" + funcion.addMinutes(30));
					trabajador.setUpdate_date(funcion.getUTC());
					sessiontrabajador.save(trabajador);
					
				}
			}
			tx.commit();

			OAuthResponse response = OAuthASResponse.tokenResponse(HttpServletResponse.SC_OK).setAccessToken(accessToken).setExpiresIn("1000")
					.setParam("access_token",trabajador.getToken_access())
					.setParam("refresh_token", trabajador.getToken_refresh())
					.setParam("usuario",trabajador.getNombre_trabajador())
					.setParam("expires_in", "1000")
					.buildJSONMessage();
			
			  return Response.status(response.getResponseStatus()).entity(response.getBody()).build();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(200).entity("Error.. ").build();
		} finally {
			sessiontrabajador.close();
		}

		//return OAuthASResponse.tokenResponse(HttpServletResponse.SC_BAD_REQUEST).buildJSONMessage();
	}

}
