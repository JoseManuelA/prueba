package com.mx.configuraciones;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

public class HibernateUtil {
		private static final SessionFactory sessionFactory;
		static {
			try {
				sessionFactory = new AnnotationConfiguration().configure("com/mx/hibernate.cfg.xml").buildSessionFactory();
				System.out.println("Hibernate Configuration loaded");
			} catch (Throwable ex) {
				// Make sure you log the exception, as it might be swallowed
				System.err.println("Initial SessionFactory creation failed." + ex);
				throw new ExceptionInInitializerError(ex);
			}
		}

		public static SessionFactory getSessionFactory() {
			return sessionFactory;
		}

	}