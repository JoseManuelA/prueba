package com.mx.entidades;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Almacen")
public class Almacen {
	@Id
	@Column(name="Id_almacen")
	private long Id_almacen;

	@Column(name="Id_suc")
	private long Id_suc;

	@Column(name="Id_producto")
	private long Id_producto;

	@Column(name="cantidad")
	private long cantidad;

	@Column(name="create_date")
	private String create_date;

	@Column(name="update_date")
	private String update_date;

	@Column(name="enable")
	private boolean enable;

	public Almacen() {
		// TODO Auto-generated constructor stub
	}

	public long getId_almacen() {
		return Id_almacen;
	}

	public void setId_almacen(long id_almacen) {
		Id_almacen = id_almacen;
	}

	public long getId_suc() {
		return Id_suc;
	}

	public void setId_suc(long id_suc) {
		Id_suc = id_suc;
	}

	public long getId_producto() {
		return Id_producto;
	}

	public void setId_producto(long id_producto) {
		Id_producto = id_producto;
	}

	public long getCantidad() {
		return cantidad;
	}

	public void setCantidad(long cantidad) {
		this.cantidad = cantidad;
	}

	public String getCreate_date() {
		return create_date;
	}

	public void setCreate_date(String create_date) {
		this.create_date = create_date;
	}

	public String getUpdate_date() {
		return update_date;
	}

	public void setUpdate_date(String update_date) {
		this.update_date = update_date;
	}

	public boolean isEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	@Override
	public String toString() {
		return "Almacen [Id_almacen=" + Id_almacen + ", Id_suc=" + Id_suc + ", Id_producto=" + Id_producto
				+ ", cantidad=" + cantidad + ", create_date=" + create_date + ", update_date=" + update_date
				+ ", enable=" + enable + "]";
	}
	
	

}
