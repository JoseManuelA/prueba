package com.mx.entidades;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Productos")
public class Producto {
	
	@Id
	@Column(name="Id_producto")
	private long Id_producto;
	
	@Column(name="nombre")
	private String nombre;
	
	@Column(name="descripcion")
	private String descripcion;
	
	@Column(name="precio_compra")
	private float precio_compra;
	
	@Column(name="precio_venta")
	private float precio_venta;
	
	@Column(name="Id_proveedor")
	private long Id_proveedor;
	
	@Column(name="create_date")
	private String create_date;
	
	@Column(name="update_date")
	private String update_date;
	
	@Column(name="enable")
	private boolean enable;

	public Producto() {
		// TODO Auto-generated constructor stub
	}

	public long getId_producto() {
		return Id_producto;
	}

	public void setId_producto(long id_producto) {
		Id_producto = id_producto;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public float getPrecio_compra() {
		return precio_compra;
	}

	public void setPrecio_compra(float precio_compra) {
		this.precio_compra = precio_compra;
	}

	public float getPrecio_venta() {
		return precio_venta;
	}

	public void setPrecio_venta(float precio_venta) {
		this.precio_venta = precio_venta;
	}

	public long getId_proveedor() {
		return Id_proveedor;
	}

	public void setId_proveedor(long id_proveedor) {
		Id_proveedor = id_proveedor;
	}

	public String getCreate_date() {
		return create_date;
	}

	public void setCreate_date(String create_date) {
		this.create_date = create_date;
	}

	public String getUpdate_date() {
		return update_date;
	}

	public void setUpdate_date(String update_date) {
		this.update_date = update_date;
	}

	public boolean getEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	@Override
	public String toString() {
		return "Producto [Id_producto=" + Id_producto + ", nombre=" + nombre + ", descripcion=" + descripcion
				+ ", precio_compra=" + precio_compra + ", precio_venta=" + precio_venta + ", Id_proveedor="
				+ Id_proveedor + ", create_date=" + create_date + ", update_date=" + update_date + ", enable=" + enable
				+ "]";
	}
	
	

}
