package trabajo;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import trabajo.HibernateUtil;
import entidades.Direccion;
import entidades.Usuarios;

public class Prueba implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Prueba() {

	}

	public static void main(String[] args) {
		// Insertar datos a la tabla direccion
		Direccion("Colonia 9", "Calle 9",5);
		// Mostrar direcciones
		// MostrarDirecciones();
		// MostrarUsuariosInnerJoin();
		// UsuarioDireccion("Juanito", "Apellido juanito", 22, "Colonia juanito", "Calle
		// juanito", 6);
		// CriteriaUpdate();ç
		//CriteriaDelete();
	}

	private static Long UsuarioDireccion(String nombre, String apellido, int edad, String colonia, String calle,
			int Id_usr) {
		Session sessiondir = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = sessiondir.beginTransaction();
		Usuarios usrs = new Usuarios();
		usrs.setNombre(nombre);
		usrs.setApellido(apellido);
		usrs.setEdad(edad);

		Direccion dire = new Direccion();
		dire.setColonia(colonia);
		dire.setCalle(calle);
		dire.setId_usr(Id_usr);

		try {
			// Se guardan los datos con save
			sessiondir.save(usrs);
			sessiondir.save(dire);
			tx.commit();

		} catch (RuntimeException e) {
			e.printStackTrace();
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			sessiondir.close();
		}

		System.out.println("Insertada direccion");
		return (long) dire.getId();
	}

	// Metodo para insertar datos en la tabla direccion
	private static Long Direccion(String colonia, String calle, int Id_usr) {
		Session sessiondir = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = sessiondir.beginTransaction();
		Direccion dire = new Direccion();
		dire.setColonia(colonia);
		dire.setCalle(calle);
		dire.setId_usr(Id_usr);

		try {
			// Se guardan los datos con save
			sessiondir.save(dire);
			tx.commit();

		} catch (RuntimeException e) {
			e.printStackTrace();
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			sessiondir.close();
		}

		System.out.println("Insertada direccion");
		return (long) dire.getId();
	}

	public static void CriteriaUpdate() {
		/*
		 * Metodo para actualizar datos con hibernate Direccion student =
		 * (Direccion)sessiondir.get(Direccion.class, 2);
		 * student.setColonia("Se cambio de Colonia tambien");
		 * 
		 */
		Session sessiondir = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = sessiondir.beginTransaction();
		Direccion student = (Direccion) sessiondir.get(Direccion.class, 2);
		student.setColonia("Se cambio de Colonia tambien");

		try {
			// Se guardan los datos con save
			// sessiondir.save(dire);
			tx.commit();

		} catch (RuntimeException e) {
			e.printStackTrace();
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			sessiondir.close();
		}

		System.out.println("Insertada update");
	}

	public static void CriteriaDelete() {
		/*
		 * Metodo para eliminar datos con hibernate Direccion student =
		 * (Direccion)sessiondir.get(Direccion.class, 2);
		 * student.setColonia("Se cambio de Colonia tambien");
		 * 
		 */
		Session sessiondir = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = sessiondir.beginTransaction();
		Direccion direccion = (Direccion) sessiondir.get(Direccion.class, 8);
		sessiondir.delete(direccion);

		try {
			// Se guardan los datos con save
			// sessiondir.save(dire);
			tx.commit();

		} catch (RuntimeException e) {
			e.printStackTrace();
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			sessiondir.close();
		}

		System.out.println("Insertada delete");
	}

	public static void MostrarUsuariosInnerJoin() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Criteria cr = session.createCriteria(Direccion.class).createAlias("usuario", "usuarios")
				.add(Restrictions.eq("usuarios.Ids", 4));
		cr.list();

		try {
			List<Direccion> list = cr.list();
			for (Iterator iterator = (list).iterator(); iterator.hasNext();) {
				Direccion dircs = (Direccion) iterator.next();
				System.out.print(" Id usuario: " + dircs.getUsuarios().getIds());
				System.out.print(" Nombre: " + dircs.getUsuarios().getNombre());
				System.out.print(" Apellido: " + dircs.getUsuarios().getApellido());
				System.out.print(" Edad: " + dircs.getUsuarios().getEdad());
				// System.out.print("Id usuario: " + dircs.getId());
				System.out.print(" Colonia: " + dircs.getColonia());
				System.out.println(" Calle: " + dircs.getCalle());
			}

		} catch (RuntimeException e) {
			e.printStackTrace();

		}
	}

	/*
	 * public static void MostrarDirecciones() { Session session =
	 * HibernateUtil.getSessionFactory().openSession(); Criteria cr =
	 * session.createCriteria(Direccion.class) //.createAlias("direccion",
	 * "direccion") .add(Restrictions.eq("Id_usr",1));
	 * //cr.add(Restrictions.eq("direccion.Id", 1)); // cr.add(nameCriterion);
	 * cr.list();
	 * 
	 * // .setFetchMode("Usuarios", FetchMode.JOIN);
	 * 
	 * List<Direccion> list = cr.list(); for (Iterator iterator = (list).iterator();
	 * iterator.hasNext();) { Direccion dircs = (Direccion) iterator.next();
	 * System.out.print("Id usuario: " + dircs.getId());
	 * System.out.print(" Colonia: " + dircs.getColonia());
	 * System.out.print(" Calle: " + dircs.getCalle());
	 * System.out.println(" Id usr: " + dircs.getId_usr());
	 * //System.out.println(" Id usr: " + dircs.getDireccion()); } }
	 */

}
