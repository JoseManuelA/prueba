package com.pruebawebapp.entidades;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import com.pruebawebapp.entidades.Usuarios;

@Entity
@Table(name="direccion")
public class Direccion {
	
	@Id
	@Column(name="Id")
	private int Id;
	
	@Column(name="colonia")
	private String colonia;
	
	@Column(name="calle")
	private String calle;

	@Column(name="Id_usr")
	private int Id_usr;
	
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "Id_usr", nullable=false,updatable=false,insertable=false)
	private Usuarios usuario;

	public Direccion() {
	
	}


	public int getId() {
		return Id;
	}


	public void setId(int id) {
		Id = id;
	}


	public String getColonia() {
		return colonia;
	}


	public void setColonia(String colonia) {
		this.colonia = colonia;
	}


	public String getCalle() {
		return calle;
	}


	public void setCalle(String calle) {
		this.calle = calle;
	}


	public int getId_usr() {
		return Id_usr;
	}


	public void setId_usr(int id_usr) {
		Id_usr = id_usr;
	}
	
	public Usuarios getUsuarios() {
		return usuario;
	}


	public void setUsuarios(Usuarios usuarios) {
		this.usuario = usuarios;
	}


	@Override
	public String toString() {
		return "Direccion [Id=" + Id + ", colonia=" + colonia + ", calle=" + calle + ", Id_usr=" + Id_usr
				+ ", usuarios="+usuario + "]";
	}

}
