package com.mx.rest;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mx.configuraciones.HibernateUtil;
import com.mx.entidades.Almacen;
import com.mx.entidades.Compras;
import com.mx.entidades.Producto;
import com.mx.entidades.Proveedores;
import com.mx.entidades.Tienda;
import com.mx.funciones.Funciones;
import com.mx.mapeadojackson.Comprajackson;

@Path("/Compra")
public class CompraService implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@POST
	@Path("/new/variado")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addTienda(List<Comprajackson> list) {
		Funciones funcion = new Funciones();
		Session sessioncompra = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = null;
		if (list.size() > 0) {
			try {
				for (Comprajackson compra : list) {
					
					tx = sessioncompra.beginTransaction();
					Comprajackson comprajackson = (Comprajackson) compra;
					Compras compras = new Compras();

					Tienda tienda = (Tienda) sessioncompra.get(Tienda.class, comprajackson.getId_suc());
					if ((tienda != null) && (tienda.isEnable()) && (tienda.getMatriz()==2)) {
						Proveedores proveedor = (Proveedores) sessioncompra.get(Proveedores.class,
								comprajackson.getId_proveedor());
						if ((proveedor != null) && (proveedor.isEnable())) {
							Producto producto = (Producto) sessioncompra.get(Producto.class,
									comprajackson.getId_producto());
							if ((producto != null) && (producto.getEnable())) {
								compras.setId_proveedor(comprajackson.getId_proveedor());
								compras.setId_producto(comprajackson.getId_producto());
								compras.setId_suc(comprajackson.getId_suc());
								compras.setPrecio_compra(comprajackson.getPrecio_compra());
								compras.setPrecio_venta(comprajackson.getPrecio_venta());
								compras.setCantidad(comprajackson.getCantidad());
								compras.setFecha(funcion.getGMT());
								compras.setCreate_date(funcion.getUTC());
								compras.setUpdate_date(funcion.getUTC());
								compras.setEnable(true);
								sessioncompra.save(compras);

								Almacen almacen = (Almacen) sessioncompra.createCriteria(Almacen.class)
										.add(Restrictions.eq("Id_producto", comprajackson.getId_producto()))
										.add(Restrictions.eq("Id_suc", comprajackson.getId_suc())).uniqueResult();

								if (almacen != null) {
									almacen.setCantidad(almacen.getCantidad() + comprajackson.getCantidad());
									almacen.setUpdate_date(funcion.getUTC());
									sessioncompra.update(almacen);
								} else {
									// Guardar como un nuevo registro en almacen
									almacen = new Almacen();
									almacen.setId_suc(comprajackson.getId_suc());
									almacen.setId_producto(comprajackson.getId_producto());
									almacen.setCantidad(comprajackson.getCantidad());
									almacen.setCreate_date(funcion.getUTC());
									almacen.setUpdate_date(funcion.getUTC());
									almacen.setEnable(true);
									sessioncompra.save(almacen);
								}
								tx.commit();
							} else {
								return Response.status(200).entity("Error en producto").build();
							}
						} else {
							return Response.status(200).entity("Error en proveedor").build();
						}

					} else {
						return Response.status(200).entity("Error en tienda").build();
					}
				}
				
				/*
				for (@SuppressWarnings("rawtypes")
				Iterator iterator = list.iterator(); iterator.hasNext();) {
			

				}
				*/
				return Response.status(200).entity("Operacion exitosa!!").build();
			} catch (RuntimeException e) {
				// TODO: handle exception
				e.printStackTrace();
				e.printStackTrace();
				if ((tx != null) && (tx.isActive())) {
					tx.rollback();
				}
			}
		}
		return Response.status(200).entity("Error ..").build();
	}
	
	@POST
	@Path("/new/")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addTienda(Comprajackson comprajackson) {
		Session sessioncompra = null;
		Transaction tx = null;
		Funciones funcion = new Funciones();
		if (comprajackson!=null) {
			try {
					sessioncompra = HibernateUtil.getSessionFactory().openSession();
					tx = sessioncompra.beginTransaction();
					Compras compras = new Compras();

					Tienda tienda = (Tienda) sessioncompra.get(Tienda.class, comprajackson.getId_suc());
					if ((tienda != null) && (tienda.isEnable()) && (tienda.getMatriz()==2)) {
						Proveedores proveedor = (Proveedores) sessioncompra.get(Proveedores.class,
								comprajackson.getId_proveedor());
						if ((proveedor != null) && (proveedor.isEnable())) {
							Producto producto = (Producto) sessioncompra.get(Producto.class,
									comprajackson.getId_producto());
							if ((producto != null) && (producto.getEnable())) {
								compras.setId_proveedor(comprajackson.getId_proveedor());
								compras.setId_producto(comprajackson.getId_producto());
								compras.setId_suc(comprajackson.getId_suc());
								compras.setPrecio_compra(comprajackson.getPrecio_compra());
								compras.setPrecio_venta(comprajackson.getPrecio_venta());
								compras.setCantidad(comprajackson.getCantidad());
								compras.setFecha(funcion.getGMT());
								compras.setCreate_date(funcion.getUTC());
								compras.setUpdate_date(funcion.getUTC());
								compras.setEnable(true);
								sessioncompra.save(compras);

								@SuppressWarnings("unchecked")
								Almacen almacen = (Almacen) sessioncompra.createCriteria(Almacen.class)
										.add(Restrictions.eq("Id_producto", comprajackson.getId_producto()))
										.add(Restrictions.eq("Id_suc", comprajackson.getId_suc())).uniqueResult();

								if (almacen != null) {
									almacen.setCantidad(almacen.getCantidad() + comprajackson.getCantidad());
									almacen.setUpdate_date(funcion.getUTC());
									sessioncompra.update(almacen);
								} else {
									// Guardar como un nuevo registro en almacen
									almacen = new Almacen();
									almacen.setId_suc(comprajackson.getId_suc());
									almacen.setId_producto(comprajackson.getId_producto());
									almacen.setCantidad(comprajackson.getCantidad());
									almacen.setCreate_date(funcion.getUTC());
									almacen.setUpdate_date(funcion.getUTC());
									almacen.setEnable(true);
									sessioncompra.save(almacen);
								}
								tx.commit();
							} else {
								return Response.status(200).entity("Error en producto").build();
							}
						} else {
							return Response.status(200).entity("Error en proveedor").build();
						}

					} else {
						return Response.status(200).entity("Error en tienda").build();
					}

				
				return Response.status(200).entity("Operacion exitosa!!").build();
			} catch (RuntimeException e) {
				// TODO: handle exception
				e.printStackTrace();
				e.printStackTrace();
				if ((tx != null) && (tx.isActive())) {
					tx.rollback();
				}
			} finally {
				sessioncompra.close();
			}
		}
		return Response.status(200).entity("Error ..").build();
	}

	@DELETE
	@Path("/delete/{id}")
	public Response deleteTienda(@PathParam("id") long id) {
		Session sessioncompra = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = sessioncompra.beginTransaction();
		Funciones funcion = new Funciones();
		try {
			Compras compra = (Compras) sessioncompra.get(Compras.class, id);
			if (compra != null) {
				if (compra.isEnable()) {
					compra.setEnable(false);
					compra.setUpdate_date(funcion.getUTC());
					// Se descontaran de almacen
					Almacen almacen = (Almacen) sessioncompra.createCriteria(Almacen.class)
							.add(Restrictions.eq("Id_producto", compra.getId_producto()))
							.add(Restrictions.eq("Id_suc", compra.getId_suc())).uniqueResult();
					if (almacen != null) {
						if ((almacen.getCantidad() - compra.getCantidad()) >= 0) {
							almacen.setCantidad(almacen.getCantidad() - compra.getCantidad());
							almacen.setUpdate_date(funcion.getUTC());
						} else {
							return Response.status(200).entity("Problemas con la transaccion").build();
						}
					} else {
						return Response.status(200).entity("Operacion exitosa!").build();
					}
					tx.commit();
					return Response.status(200).entity("Oepracion exitosa!!").build();
				} else {
					return Response.status(200).entity("No se pudo efectuar la operacion").build();
				}
			} else {
				return Response.status(200).entity("Error...!!").build();
			}
		} catch (RuntimeException e) {
			e.printStackTrace();
			return Response.status(200).entity("Error: " + e).build();
		} finally {
			sessioncompra.close();
		}
	}
	
	@GET
	@Path("/search/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response searchTienda(@PathParam("id") long id) {
		Session sessiontienda = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = sessiontienda.beginTransaction();
		try {
			ObjectMapper mapper = new ObjectMapper();
			String jsonStr = "";
			Compras compra = (Compras) sessiontienda.get(Compras.class, id);
			if (compra != null) {

				if (compra.isEnable()) {
					jsonStr += mapper.writerWithDefaultPrettyPrinter().writeValueAsString(compra);
					return Response.status(200).entity(jsonStr).build();
				} else {
					return Response.status(200).entity("No se puede efectuar esta operacion").build();
				}
			}else {
				return Response.status(200).entity("Error ....").build();
			}
		} catch (RuntimeException e) {
			e.printStackTrace();
			if ((tx != null) && (tx.isActive())) {
				tx.rollback();
			}
			return Response.status(200).entity("Error: " + e).build();
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(200).entity("No se puede efectuar esta operacion").build();
		}
	}
	
	@GET
	@Path("/search/")
	@Produces(MediaType.APPLICATION_JSON)
	public Response searchVentaTotal() {
		Session sessionventa = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = sessionventa.beginTransaction();
		try {
			@SuppressWarnings("unchecked")
			List<Compras> list = (List<Compras>) sessionventa
					.createCriteria(Compras.class).list();
			ObjectMapper mapper = new ObjectMapper();
			String jsonStr = "";
			for (Iterator iterator = list.iterator(); iterator.hasNext();) {
				Compras compras = (Compras) iterator.next();
				jsonStr += mapper.writerWithDefaultPrettyPrinter().writeValueAsString(compras);
			}
			return Response.status(200).entity(jsonStr).build();
		} catch (RuntimeException e) {
			e.printStackTrace();
			if ((tx != null) && (tx.isActive())) {
				tx.rollback();
			}
			return Response.status(200).entity("Error: " + e).build();
		}
		catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(200).entity("Error en la operacion").build();
		}
	}
}
