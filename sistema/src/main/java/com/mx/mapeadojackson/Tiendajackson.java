package com.mx.mapeadojackson;

public class Tiendajackson {
	private long Id_suc;
	private String nombre;
	private String colonia;
	private String calle;
	private String cp;
	private String numero;
	private String telefono;
	private long matriz;
	private String create_date;
	private String update_date;
	private boolean enable;
	

	public Tiendajackson() {
		// TODO Auto-generated constructor stub
	}


	public long getId_suc() {
		return Id_suc;
	}


	public void setId_suc(long id_suc) {
		Id_suc = id_suc;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getColonia() {
		return colonia;
	}


	public void setColonia(String colonia) {
		this.colonia = colonia;
	}


	public String getCalle() {
		return calle;
	}


	public void setCalle(String calle) {
		this.calle = calle;
	}


	public String getCp() {
		return cp;
	}


	public void setCp(String cp) {
		this.cp = cp;
	}


	public String getNumero() {
		return numero;
	}


	public void setNumero(String numero) {
		this.numero = numero;
	}


	public String getTelefono() {
		return telefono;
	}


	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}


	public long getMatriz() {
		return matriz;
	}


	public void setMatriz(long matriz) {
		this.matriz = matriz;
	}


	public String getCreate_date() {
		return create_date;
	}


	public void setCreate_date(String create_date) {
		this.create_date = create_date;
	}


	public String getUpdate_date() {
		return update_date;
	}


	public void setUpdate_date(String update_date) {
		this.update_date = update_date;
	}


	public boolean isEnable() {
		return enable;
	}


	public void setEnable(boolean enable) {
		this.enable = enable;
	}


	@Override
	public String toString() {
		return "Tiendajackson [Id_suc=" + Id_suc + ", nombre=" + nombre + ", colonia=" + colonia + ", calle=" + calle
				+ ", cp=" + cp + ", numero=" + numero + ", telefono=" + telefono + ", matriz=" + matriz
				+ ", create_date=" + create_date + ", update_date=" + update_date + ", enable=" + enable + "]";
	}
	
	

}
