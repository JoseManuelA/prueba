package com.mx.mapeadojackson;

import java.util.List;

import com.mx.entidades.Transferencias;

public class ReporteTransjackson {
	
	private long Id_transferencia;
	private long transferenciaR;
	private long transferenciaE;
	private long Id_suc;
	private String fecha;
	private long origen;
	private long destino;
	private List<Transferencias> transferencias;
	private long total_transferencia;
	
	

	public long getOrigen() {
		return origen;
	}



	public void setOrigen(long origen) {
		this.origen = origen;
	}



	public long getDestino() {
		return destino;
	}



	public void setDestino(long destino) {
		this.destino = destino;
	}



	public long getId_transferencia() {
		return Id_transferencia;
	}



	public void setId_transferencia(long id_transferencia) {
		Id_transferencia = id_transferencia;
	}



	public long getTransferenciaR() {
		return transferenciaR;
	}



	public void setTransferenciaR(long transferenciaR) {
		this.transferenciaR = transferenciaR;
	}



	public long getTransferenciaE() {
		return transferenciaE;
	}



	public void setTransferenciaE(long transferenciaE) {
		this.transferenciaE = transferenciaE;
	}



	public long getId_suc() {
		return Id_suc;
	}



	public void setId_suc(long id_suc) {
		Id_suc = id_suc;
	}



	public String getFecha() {
		return fecha;
	}



	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	

	public List<Transferencias> getTransferencias() {
		return transferencias;
	}



	public void setTransferencias(List<Transferencias> transferencias) {
		this.transferencias = transferencias;
	}



	public long getTotal_transferencia() {
		return total_transferencia;
	}



	public void setTotal_transferencia(long total_transferencia) {
		this.total_transferencia = total_transferencia;
	}



	public ReporteTransjackson() {
		// TODO Auto-generated constructor stub
	}

}
