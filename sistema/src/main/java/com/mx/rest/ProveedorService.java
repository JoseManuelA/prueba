package com.mx.rest;

import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mx.configuraciones.HibernateUtil;
import com.mx.entidades.Proveedores;
import com.mx.mapeadojackson.Provedorjackson;

@Path("/Proveedor")
/**
 * 
 * @author 
 *
 */
public class ProveedorService implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ProveedorService() {
		// TODO Auto-generated constructor stub
	}


	/***
	 * 
	 * @param proveedorjson
	 * @return
	 */
	@POST
	@Path("/new/")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addProveedor(Provedorjackson proveedorjson) {
		Session sessionproveedor = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = sessionproveedor.beginTransaction();
		try {
			Proveedores proveedor = new Proveedores();
			if (proveedorjson.getNombre() != null) {
				proveedor.setNombre(proveedorjson.getNombre());
			}
			if (proveedorjson.getTelefono() != null) {
				proveedor.setTelefono(proveedorjson.getTelefono());
			}
			proveedor.setcreate_date(ObtenerUTC());
			proveedor.setUpdate_date(ObtenerUTC());
			proveedor.setEnable(true);
			sessionproveedor.save(proveedor);
			tx.commit();
			return Response.status(200).entity("Oepracion exitosa!!").build();
		} catch (RuntimeException e) {
			// TODO: handle exception
			e.printStackTrace();
			return Response.status(200).entity("Error: " + e).build();
		}

	}

	@GET
	@Path("/search/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response searchProveedor(@PathParam("id") long id) {
		Session sessionproveedor = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = sessionproveedor.beginTransaction();
		try {
			ObjectMapper mapper = new ObjectMapper();
			String jsonStr = "";
			Proveedores proveedor = (Proveedores) sessionproveedor.get(Proveedores.class, id);
			if (proveedor != null) {

				if (proveedor.isEnable()) {
					jsonStr += mapper.writerWithDefaultPrettyPrinter().writeValueAsString(proveedor);
					return Response.status(200).entity(jsonStr).build();
				} else {
					return Response.status(200).entity("No se puede efectuar esta operacion").build();
				}
			}else {
				return Response.status(200).entity("Error ....").build();
			}
		} catch (RuntimeException e) {
			e.printStackTrace();
			if ((tx != null) && (tx.isActive())) {
				tx.rollback();
			}
			return Response.status(200).entity("Error: " + e).build();
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return Response.status(200).entity("No se puede efectuar esta operacion").build();
	}
	
	
	@DELETE
	@Path("/delete/{id}")
	public Response deleteProveedor(@PathParam("id") long id) {
		Session sessionproveedor = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = sessionproveedor.beginTransaction();
		try {
			Proveedores proveedor = (Proveedores) sessionproveedor.get(Proveedores.class, id);
			if (proveedor != null) {
				if (proveedor.isEnable()) {
					proveedor.setEnable(false);
					tx.commit();
					return Response.status(200).entity("Oepracion exitosa!!").build();
				} else {
					return Response.status(200).entity("No se pudo efectuar la operacion").build();
				}
			} else {
				return Response.status(200).entity("Error...!!").build();
			}
		} catch (RuntimeException e) {
			e.printStackTrace();
			return Response.status(200).entity("Error: " + e).build();
		}
	}
	
	@PUT
	@Path("/update/")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateProveedor(Provedorjackson proveedorjson) {
		Session sessionproveedor = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = sessionproveedor.beginTransaction();

		try {
			ObjectMapper mapper = new ObjectMapper();
			String jsonStr = "";
			Proveedores proveedor = (Proveedores) sessionproveedor.get(Proveedores.class, proveedorjson.getId_proveedor());
			if (proveedor != null) {
				if (proveedor.isEnable()) {
					if (proveedorjson.getNombre() != null) {
						proveedor.setNombre(proveedorjson.getNombre());
					}
					if (proveedorjson.getTelefono() != null) {
						proveedor.setTelefono(proveedorjson.getTelefono());
					}
					proveedor.setUpdate_date(ObtenerUTC());
					proveedor.setEnable(true);
					jsonStr += mapper.writerWithDefaultPrettyPrinter().writeValueAsString(proveedor);
					//contenido = proveedor.toString();
					tx.commit();
					return Response.status(200).entity(jsonStr).build();
				} else {
					return Response.status(200).entity("No se puedo efectuar la operacion ").build();
				}
			} else {
				return Response.status(200).entity("No se encontro proveedor con los datos ingresados ").build();
			}
		} catch (RuntimeException e) {
			e.printStackTrace();
			if ((tx != null) && (tx.isActive())) {
				tx.rollback();
			}
			return Response.status(200).entity("Error: " + e).build();
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return Response.status(200).entity("No se puedo efectuar la operacion ").build();
	}
	
	public static String ObtenerUTC() {
		final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
		final String utcTime = sdf.format(new Date());

		return utcTime;
	}
}
