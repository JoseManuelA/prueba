package com.mx.rest;

import java.io.IOException;
import java.io.Serializable;
import java.security.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mx.configuraciones.HibernateUtil;
import com.mx.entidades.Almacen;
import com.mx.entidades.Producto;
import com.mx.entidades.Tienda;
import com.mx.entidades.Tipo_trabajador;
import com.mx.entidades.Trabajadores;
import com.mx.entidades.Ventas;
import com.mx.funciones.Funciones;
import com.mx.mapeadojackson.Autentificacion;
import com.mx.mapeadojackson.Ventajackson;

@Path("/Venta")
public class VentaService implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public VentaService() {
		// TODO Auto-generated constructor stub
	}
	public static Response error = Response.status(500).entity("Se producio un error..").build(); 
	public static Response correcto = Response.status(201).entity("Se efectuo la operacion").build();
	
	@POST
	@Path("/new/variado")
	//@Consumes(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response addVenta2(@Context HttpServletRequest request) {
		Session sessionventa =sessionventa = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = null;
		//if (list.size() > 0) {
			try {
				String contenido = request.getHeader("autorizacion");
				String token = request.getHeader("token");
				Funciones funcion = new Funciones();
				
				
				@SuppressWarnings("unchecked")
				List<Ventajackson> listventa =(List<Ventajackson>) new ObjectMapper().readValue(contenido, new TypeReference<List<Ventajackson>>(){});
				
				for (Ventajackson ventajackson : listventa) {
					tx = sessionventa.beginTransaction();
					// Verificar si la tienda esat habilitada
					Tienda tienda = (Tienda) sessionventa.get(Tienda.class, ventajackson.getId_suc());
					if ((tienda != null) && (tienda.isEnable())) {

						// verificar si el trabajdor lo puede vender
						Trabajadores trabajador = (Trabajadores) sessionventa.get(Trabajadores.class,
								ventajackson.getId_trabajador());
						if ((trabajador != null) && (trabajador.getEnable())
								&& (trabajador.getId_suc() == tienda.getId_suc())) {
							if (trabajador.getToken_access().equals(token) && (trabajador.getFecha_access().compareTo(funcion.getGMT())>0)) {
								Tipo_trabajador tipo = (Tipo_trabajador) sessionventa.get(Tipo_trabajador.class,
										trabajador.getId_tipo());
								if ((tipo != null) && (tipo.getPuesto().equals("Vendedor")) && (tipo.isEnable())) {

									Ventas venta = new Ventas();
									venta.setId_suc(ventajackson.getId_suc());
									venta.setId_producto(ventajackson.getId_producto());
									venta.setId_trabajador(ventajackson.getId_trabajador());
									venta.setCantidad(ventajackson.getCantidad());
									venta.setTotal(ventajackson.getTotal());
									venta.setFecha(funcion.getGMT());
									venta.setCreate_date(funcion.getUTC());
									venta.setUpdate_date(funcion.getUTC());
									venta.setEnable(true);
									sessionventa.save(venta);

									Producto producto = (Producto) sessionventa.get(Producto.class,
											ventajackson.getId_producto());
									if ((producto != null) && (producto.getEnable())) {

										@SuppressWarnings("unchecked")
										Almacen almacen = (Almacen) sessionventa.createCriteria(Almacen.class)
												.add(Restrictions.eq("Id_producto", ventajackson.getId_producto()))
												.add(Restrictions.eq("Id_suc", ventajackson.getId_suc())).uniqueResult();

										if (almacen != null) {
											almacen.setCantidad(almacen.getCantidad() - ventajackson.getCantidad());
											almacen.setUpdate_date(funcion.getUTC());
											sessionventa.flush();
											sessionventa.clear();
											sessionventa.update(almacen);
										} else {
											// No se tiene disponibilidad del producto
											return Response.status(500).entity("error en Almacen").build();
										}
									} else {
										return Response.status(500).entity("error en Producto").build();
									}
							
									tx.commit();

								} else {
									return Response.status(500).entity("error en tipo trabajador").build();
								}
							}

					

						} else {
							return Response.status(500).entity("error en trabajador").build();
						}

					} else {
						return Response.status(500).entity("erro en Tienda").build();
					}
				}
				return correcto;
				//return Response.status(200).entity("Se efectuaron las operaciones").build();
			} catch (RuntimeException e) {
				// TODO: handle exception
				e.printStackTrace();
				e.printStackTrace();
				if ((tx != null) && (tx.isActive())) {
					tx.rollback();
				}
				return Response.status(200).entity("Error "+e).build();
			} catch (JsonParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return Response.status(200).entity("Error "+e).build();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return Response.status(200).entity("Error "+e).build();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return Response.status(200).entity("Error "+e).build();
			} finally {
				sessionventa.close();
			}
		//}
			//return Response.status(200).entity("Error ").build();
	}
	
	@POST
	@Path("/new/")
	//@Consumes(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response addVenta(@Context HttpServletRequest request) {
		Session sessionventa = null;
		Transaction tx = null;
		//if (ventajackson!=null) {
			try {
				Funciones funcion = new Funciones();
				String contenido = request.getHeader("autorizacion");
				String token = request.getHeader("token");
				//Autentificacion autentificacion = new ObjectMapper().readValue(token, Autentificacion.class);
				Ventajackson ventajackson = new ObjectMapper().readValue(contenido, Ventajackson.class);
					sessionventa = HibernateUtil.getSessionFactory().openSession();
					tx = sessionventa.beginTransaction();

					// Verificar si la tienda esat habilitada
					Tienda tienda = (Tienda) sessionventa.get(Tienda.class, ventajackson.getId_suc());
					if ((tienda != null) && (tienda.isEnable())) {

						// verificar si el trabajdor lo puede vender
						Trabajadores trabajador = (Trabajadores) sessionventa.get(Trabajadores.class,
								ventajackson.getId_trabajador());
						if ((trabajador != null) && (trabajador.getEnable())
								&& (trabajador.getId_suc() == tienda.getId_suc()) && (trabajador.getToken_access()!=null)) {						   					   
							if (trabajador.getToken_access().equals(token) && (trabajador.getFecha_access().compareTo(funcion.getGMT())>0)) {

								Tipo_trabajador tipo = (Tipo_trabajador) sessionventa.get(Tipo_trabajador.class,
										trabajador.getId_tipo());
								if ((tipo != null) && (tipo.getPuesto().equals("Vendedor")) && (tipo.isEnable())) {

									Ventas venta = new Ventas();
									venta.setId_suc(ventajackson.getId_suc());
									venta.setId_producto(ventajackson.getId_producto());
									venta.setId_trabajador(ventajackson.getId_trabajador());
									venta.setCantidad(ventajackson.getCantidad());
									venta.setTotal(ventajackson.getTotal());
									venta.setFecha(funcion.getGMT());
									venta.setCreate_date(funcion.getUTC());
									venta.setUpdate_date(funcion.getUTC());
									venta.setEnable(true);
									sessionventa.save(venta);

									Producto producto = (Producto) sessionventa.get(Producto.class,
											ventajackson.getId_producto());
									if ((producto != null) && (producto.getEnable())) {

										Almacen almacen = (Almacen) sessionventa.createCriteria(Almacen.class)
												.add(Restrictions.eq("Id_producto", ventajackson.getId_producto()))
												.add(Restrictions.eq("Id_suc", ventajackson.getId_suc())).uniqueResult();

										if (almacen != null) {
											almacen.setCantidad(almacen.getCantidad() - ventajackson.getCantidad());
											almacen.setUpdate_date(funcion.getUTC());
											sessionventa.update(almacen);
										} else {
											// No se tiene disponibilidad del producto
											return Response.status(200).entity("error en Almacen").build();
										}
									} else {
										return Response.status(200).entity("error en Producto").build();
									}
									tx.commit();

								}else {
									return Response.status(200).entity("error en tipo trabajador").build();
								} 
							}else {
								return Response.status(500).entity("error en el token y la fecha").build();
							}
						} else {
							return Response.status(200).entity("error en trabajador").build();
						}

					} else {
						return Response.status(200).entity("erro en Tienda").build();
					}
					return Response.status(200).entity("Se efectuo la operacion").build();
			} catch (RuntimeException e) {
				// TODO: handle exception
				e.printStackTrace();
				e.printStackTrace();
				if ((tx != null) && (tx.isActive())) {
					tx.rollback();
				}
			} catch (JsonParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				sessionventa.close();
			}
		//}
		return Response.status(500).entity("Error no se efectuo la operacion").build();
	}
	
	
	@DELETE
	@Path("/delete/{id}")
	public Response deleteTienda(@PathParam("id") long id) {
		Session sessionventa = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = sessionventa.beginTransaction();
		try {
			Funciones funcion = new Funciones();
			Ventas venta = (Ventas) sessionventa.get(Ventas.class, id);
			if (venta != null) {
				if (venta.isEnable()) {
					venta.setEnable(false);
					venta.setUpdate_date(funcion.getUTC());
					// Se descontaran de almacen
					Almacen almacen = (Almacen) sessionventa.createCriteria(Almacen.class)
							.add(Restrictions.eq("Id_producto", venta.getId_producto()))
							.add(Restrictions.eq("Id_suc", venta.getId_suc())).uniqueResult();
					if (almacen != null) {
						if ((almacen.getCantidad() - venta.getCantidad()) >= 0) {
							almacen.setCantidad(almacen.getCantidad() + venta.getCantidad());
							almacen.setUpdate_date(funcion.getUTC());
						} else {
							return Response.status(200).entity("Problemas con la transaccion").build();
						}
					} else {
						return Response.status(200).entity("Operacion exitosa!").build();
					}
					tx.commit();
					return Response.status(200).entity("Oepracion exitosa!!").build();
				} else {
					return Response.status(200).entity("No se pudo efectuar la operacion").build();
				}
			} else {
				return Response.status(200).entity("Error...!!").build();
			}
		} catch (RuntimeException e) {
			// TODO: handle exception
			e.printStackTrace();
			e.printStackTrace();
			if ((tx != null) && (tx.isActive())) {
				tx.rollback();
			}
		} finally {
			sessionventa.close();
		}
		return Response.status(200).entity("error en la operacion").build();
	}
	
	@GET
	@Path("/search/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response searchTienda(@PathParam("id") long id) {
		Session sessionventa = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = sessionventa.beginTransaction();
		try {
			ObjectMapper mapper = new ObjectMapper();
			String jsonStr = "";
			Ventas ventas = (Ventas) sessionventa.get(Ventas.class, id);
			if (ventas != null) {

				if (ventas.isEnable()) {
					jsonStr += mapper.writerWithDefaultPrettyPrinter().writeValueAsString(ventas);
					return Response.status(200).entity(jsonStr).build();
				} else {
					return Response.status(200).entity("No se puede efectuar esta operacion").build();
				}
			}else {
				return Response.status(200).entity("Error ....").build();
			}
		} catch (RuntimeException e) {
			e.printStackTrace();
			if ((tx != null) && (tx.isActive())) {
				tx.rollback();
			}
			return Response.status(200).entity("Error: " + e).build();
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return Response.status(200).entity("No se puede efectuar esta operacion").build();
	}
	
	@GET
	@Path("/search")
	@Produces(MediaType.APPLICATION_JSON)
	public Response searchVentaTotal() {
		Session sessionventa = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = sessionventa.beginTransaction();
		try {
			@SuppressWarnings("unchecked")
			List<Ventas> list = (List<Ventas>) sessionventa
					.createCriteria(Ventas.class).list();
			ObjectMapper mapper = new ObjectMapper();
			String jsonStr = "";
			for (Ventas ventas : list) {
				jsonStr += mapper.writerWithDefaultPrettyPrinter().writeValueAsString(ventas);
			}
			return Response.status(200).entity(jsonStr).build();
		} catch (RuntimeException e) {
			e.printStackTrace();
			if ((tx != null) && (tx.isActive())) {
				tx.rollback();
			}
			return Response.status(200).entity("Error: " + e).build();
		}
		catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return Response.status(200).entity("Error en la operacion").build();
	}


}
