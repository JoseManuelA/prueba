package com.mx.mapeadojackson;

import java.util.List;

import com.mx.entidades.Ventas;

public class ReporteVentajackson {
	

	private long Id_suc;
	private long cantidad;
	//private long precio_compra;
	//private long precio_venta;
	private List<Ventas> ventas;
	
	

	public long getId_suc() {
		return Id_suc;
	}



	public void setId_suc(long id_suc) {
		Id_suc = id_suc;
	}



	public long getCantidad() {
		return cantidad;
	}



	public void setCantidad(long cantidad) {
		this.cantidad = cantidad;
	}


/*
	public long getPrecio_compra() {
		return precio_compra;
	}



	public void setPrecio_compra(long precio_compra) {
		this.precio_compra = precio_compra;
	}



	public long getPrecio_venta() {
		return precio_venta;
	}



	public void setPrecio_venta(long precio_venta) {
		this.precio_venta = precio_venta;
	}

*/

	public List<Ventas> getVentas() {
		return ventas;
	}



	public void setVentas(List<Ventas> ventas) {
		this.ventas = ventas;
	}



	public ReporteVentajackson() {
		// TODO Auto-generated constructor stub
	}

}
