-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 02, 2019 at 12:49 AM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `almacen`
--

-- --------------------------------------------------------

--
-- Table structure for table `Almacen`
--

CREATE TABLE `Almacen` (
  `Id_almacen` int(11) NOT NULL,
  `Id_suc` int(11) NOT NULL,
  `Id_producto` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `enable` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Almacen`
--

INSERT INTO `Almacen` (`Id_almacen`, `Id_suc`, `Id_producto`, `cantidad`, `create_date`, `update_date`, `enable`) VALUES
(21, 8, 10, 80, '2019-02-28 06:06:34', '2019-02-28 06:34:41', 1),
(22, 8, 11, 36, '2019-02-28 06:06:34', '2019-02-28 06:34:41', 1),
(23, 8, 12, 25, '2019-02-28 06:06:34', '2019-02-28 06:36:39', 1),
(24, 8, 13, 102, '2019-02-28 06:06:34', '2019-02-28 06:36:39', 1),
(25, 8, 14, 320, '2019-02-28 06:06:34', '2019-02-28 06:28:45', 1),
(26, 9, 12, 2, '2019-02-28 06:07:26', '2019-02-28 06:38:14', 1),
(27, 9, 13, 6, '2019-02-28 06:07:26', '2019-02-28 06:38:15', 1),
(28, 10, 12, 8, '2019-03-01 22:58:13', '2019-03-02 05:14:26', 1),
(29, 10, 13, 7, '2019-03-01 22:58:08', '2019-03-02 05:14:26', 1);

-- --------------------------------------------------------

--
-- Table structure for table `Compras`
--

CREATE TABLE `Compras` (
  `Id_compra` int(11) NOT NULL,
  `Id_proveedor` int(11) NOT NULL,
  `Id_producto` int(11) NOT NULL,
  `Id_suc` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio_compra` int(11) NOT NULL,
  `precio_venta` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `enable` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Compras`
--

INSERT INTO `Compras` (`Id_compra`, `Id_proveedor`, `Id_producto`, `Id_suc`, `cantidad`, `precio_compra`, `precio_venta`, `fecha`, `create_date`, `update_date`, `enable`) VALUES
(45, 1, 10, 8, 20, 5, 6, '2019-02-28 00:06:34', '2019-02-28 06:06:34', '2019-02-28 06:06:34', 1),
(46, 1, 11, 8, 20, 7, 8, '2019-02-28 00:06:34', '2019-02-28 06:06:34', '2019-02-28 06:06:34', 1),
(47, 2, 12, 8, 20, 9, 10, '2019-02-28 00:06:34', '2019-02-28 06:06:34', '2019-02-28 06:06:34', 1),
(48, 3, 13, 8, 20, 21, 22, '2019-02-28 00:06:34', '2019-02-28 06:06:34', '2019-02-28 06:06:34', 1),
(49, 4, 14, 8, 20, 11, 12, '2019-02-28 00:06:34', '2019-02-28 06:06:34', '2019-02-28 06:06:34', 1),
(50, 1, 10, 8, 5, 5, 6, '2019-02-28 00:07:40', '2019-02-28 06:07:40', '2019-02-28 06:07:40', 1),
(51, 1, 11, 8, 8, 7, 8, '2019-02-28 00:07:40', '2019-02-28 06:07:40', '2019-02-28 06:07:40', 1),
(52, 2, 12, 8, 11, 9, 10, '2019-02-28 00:07:40', '2019-02-28 06:07:40', '2019-02-28 06:07:40', 1),
(53, 1, 10, 8, 50, 5, 6, '2019-02-28 00:28:45', '2019-02-28 06:28:45', '2019-02-28 06:28:45', 1),
(54, 3, 13, 8, 100, 21, 22, '2019-02-28 00:28:45', '2019-02-28 06:28:45', '2019-02-28 06:28:45', 1),
(55, 4, 14, 8, 300, 11, 12, '2019-02-28 00:28:45', '2019-02-28 06:28:45', '2019-02-28 06:28:45', 1),
(56, 1, 10, 8, 5, 5, 6, '2019-02-28 00:34:41', '2019-02-28 06:34:41', '2019-02-28 06:34:41', 1),
(57, 1, 11, 8, 8, 7, 8, '2019-02-28 00:34:41', '2019-02-28 06:34:41', '2019-02-28 06:34:41', 1),
(58, 2, 12, 8, 11, 9, 10, '2019-02-28 00:34:41', '2019-02-28 06:34:41', '2019-02-28 06:34:41', 1);

-- --------------------------------------------------------

--
-- Table structure for table `Productos`
--

CREATE TABLE `Productos` (
  `Id_producto` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `descripcion` text NOT NULL,
  `precio_compra` double NOT NULL,
  `precio_venta` double NOT NULL,
  `Id_proveedor` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `enable` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Productos`
--

INSERT INTO `Productos` (`Id_producto`, `nombre`, `descripcion`, `precio_compra`, `precio_venta`, `Id_proveedor`, `create_date`, `update_date`, `enable`) VALUES
(1, 'Producto 1', 'Descripcion producto1', 15, 20, 1, '2019-02-22 17:34:47', '2019-02-25 23:00:41', 1),
(2, 'Producto 2', 'Descripcion producto 2', 15, 20, 1, '2019-02-22 17:36:48', '2019-02-25 22:59:52', 1),
(3, 'Producto 3', 'Descripcion producto 4', 15, 20, 1, '2019-02-22 17:38:05', '2019-02-25 23:00:00', 1),
(4, 'Producto 4 nuevo', 'Descripcion prodcto 4 nuevo', 17, 22, 1, '2019-02-22 17:39:52', '2019-02-27 21:56:49', 1),
(5, 'Producto 5', 'Descripcion producto 5', 15, 20, 1, '2019-02-22 17:45:07', '2019-02-25 23:00:27', 1),
(6, 'Producto 6', 'Descripcion producto 6', 15, 20, 1, '2019-02-22 17:47:40', '2019-02-25 23:00:33', 1),
(7, 'Producto nuevo 7', 'Descripcion producto  nuevo 7', 5, 50, 1, '2019-02-22 17:48:23', '2019-02-22 17:48:23', 1),
(8, 'Producto nuevo 7', 'Descripcion producto  nuevo 7', 5, 50, 1, '2019-02-23 04:30:57', '2019-02-25 15:37:29', 1),
(9, 'Producto XZ', 'Descripcion XZ', 15, 20, 1, '2019-02-27 21:54:34', '2019-02-27 15:55:28', 1),
(10, 'Producto 1', 'Producto 1', 5, 6, 1, '2019-02-28 03:02:12', '2019-02-28 03:02:12', 1),
(11, 'Producto 2', 'Producto 2', 7, 8, 1, '2019-02-28 03:02:40', '2019-02-28 03:02:40', 1),
(12, 'Producto 3', 'Producto 3', 9, 10, 2, '2019-02-28 03:03:02', '2019-02-28 03:03:02', 1),
(13, 'Producto 4', 'Producto 4', 21, 22, 3, '2019-02-28 03:03:36', '2019-02-28 03:03:36', 1),
(14, 'Producto 5', 'Producto 5', 11.25, 12, 4, '2019-02-28 03:04:02', '2019-02-27 21:08:25', 1),
(15, 'Producto 6', 'Producto 6', 11.25, 12.25, 4, '2019-02-28 03:08:12', '2019-02-27 21:08:32', 0);

-- --------------------------------------------------------

--
-- Table structure for table `Proveedores`
--

CREATE TABLE `Proveedores` (
  `Id_proveedor` int(11) NOT NULL,
  `nombre` varchar(60) NOT NULL,
  `telefono` varchar(15) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `enable` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Proveedores`
--

INSERT INTO `Proveedores` (`Id_proveedor`, `nombre`, `telefono`, `create_date`, `update_date`, `enable`) VALUES
(1, 'proveedor renovada S.A de C.V', '555555555', '2019-02-22 19:23:17', '2019-02-23 01:55:02', 1),
(2, 'proveedor 2 S.A de C.V', '34582469', '2019-02-22 19:37:42', '2019-02-22 19:37:42', 1),
(3, 'proveedor renovada S.A de C.V', '34582469', '2019-02-23 04:33:51', '2019-02-27 21:13:16', 1),
(4, 'Nuevo Proveedor S.A de C.V', '01800551056', '2019-02-27 21:46:36', '2019-02-27 15:48:43', 1);

-- --------------------------------------------------------

--
-- Table structure for table `Tienda`
--

CREATE TABLE `Tienda` (
  `Id_suc` int(11) NOT NULL,
  `nombre` varchar(60) NOT NULL,
  `colonia` text NOT NULL,
  `calle` text NOT NULL,
  `cp` varchar(15) NOT NULL,
  `numero` varchar(10) NOT NULL,
  `telefono` text NOT NULL,
  `matriz` int(1) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `enable` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Tienda`
--

INSERT INTO `Tienda` (`Id_suc`, `nombre`, `colonia`, `calle`, `cp`, `numero`, `telefono`, `matriz`, `create_date`, `update_date`, `enable`) VALUES
(2, 'La tiendita de la esquina', 'ventura puente', 'puente chico', '5210', '3333333', '1111111', 1, '2019-02-21 19:15:27', '2019-02-27 21:36:34', 1),
(3, 'latiendita', 'villas', 'callevilla', '58210', '141A', '3133149', 1, '2019-02-21 18:25:51', '2019-02-21 18:25:51', 1),
(4, 'la tiendita Central', 'villas del pedregal', 'callevilla', '58210', '125A', '3133157', 2, '2019-02-21 19:32:55', '2019-02-25 15:38:03', 1),
(5, 'latiendita nueva av solidaridad', 'villas', 'callevilla', '58210', '141A', '3133149', 1, '2019-02-22 16:26:35', '2019-02-22 16:26:35', 1),
(6, 'Abarrotes Azteca', 'El plan', 'Calle uno', '52100', '98954565', '2222222', 1, '2019-02-27 21:38:30', '2019-02-27 15:41:04', 1),
(7, 'Prueba', 'Colonia', 'Calle', '58100', '54', '3452585', 2, '2019-02-28 02:56:33', '2019-02-28 02:56:33', 1),
(8, 'A', 'Colonia', 'Calle', '58100', '54', '3452585', 2, '2019-02-28 02:58:02', '2019-02-28 02:58:02', 1),
(9, 'B', 'Colonia', 'Calle', '58100', '54', '3452599', 1, '2019-02-28 02:58:44', '2019-02-28 02:58:44', 1),
(10, 'C', 'Colonia', 'Calle', '58100', '59', '3452585', 1, '2019-02-28 02:59:01', '2019-02-28 02:59:01', 1);

-- --------------------------------------------------------

--
-- Table structure for table `Tipo_trabajador`
--

CREATE TABLE `Tipo_trabajador` (
  `Id_tipo` int(11) NOT NULL,
  `puesto` varchar(50) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `enable` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Tipo_trabajador`
--

INSERT INTO `Tipo_trabajador` (`Id_tipo`, `puesto`, `create_date`, `update_date`, `enable`) VALUES
(1, 'Gerente', '2019-02-22 21:08:05', '2019-02-20 19:15:27', 1),
(2, 'Vendedor', '2019-02-22 21:08:43', '2019-02-20 19:19:08', 1),
(3, 'Almacen', '2019-02-22 21:09:02', '2019-02-20 19:27:15', 1);

-- --------------------------------------------------------

--
-- Table structure for table `Trabajadores`
--

CREATE TABLE `Trabajadores` (
  `Id_trabajador` int(11) NOT NULL,
  `Id_suc` int(11) NOT NULL,
  `nombre_trabajador` varchar(50) NOT NULL,
  `apellidop_trabajador` varchar(90) NOT NULL,
  `apellidom_trabajador` varchar(50) NOT NULL,
  `Id_tipo` int(11) NOT NULL,
  `password` text NOT NULL,
  `token_access` text NOT NULL,
  `fecha_access` text,
  `token_refresh` text NOT NULL,
  `fecha_refresh` text,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `enable` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Trabajadores`
--

INSERT INTO `Trabajadores` (`Id_trabajador`, `Id_suc`, `nombre_trabajador`, `apellidop_trabajador`, `apellidom_trabajador`, `Id_tipo`, `password`, `token_access`, `fecha_access`, `token_refresh`, `fecha_refresh`, `create_date`, `update_date`, `enable`) VALUES
(1, 3, 'Jose manuel', 'ALvarez', 'Bucio', 3, '3336558', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '2019-02-21 23:21:18', '2019-02-26 17:49:36', 0),
(2, 3, 'Jose Manuel', 'Alvarez', 'Bucio', 2, '3336558', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '2019-02-21 20:10:32', '2019-02-27 21:27:35', 1),
(3, 4, 'Juan Antonio', 'Hernandez', 'Guitierrez', 3, '12325485', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '2019-02-23 02:28:54', '2019-02-25 20:47:28', 1),
(4, 4, 'Antonio', 'Sanchez', 'Guitierrez', 2, '12325485', '', '', '', '', '2019-02-23 04:07:59', '2019-03-01 19:16:56', 1),
(5, 2, 'Gilberto', 'Saul', 'Garibay', 1, '12325485', '', '', '', '', '2019-02-23 04:09:33', '2019-03-01 19:16:49', 1),
(6, 3, 'Fernando', 'Gilberto', 'Cruz', 3, '3336558', '', '', '', '', '2019-02-27 21:29:16', '2019-03-01 19:16:43', 1),
(7, 8, 'JOSE MANUEL', 'ALVAREZ', 'BUCIO', 2, '12345678', '', '', '', '', '2019-02-28 03:20:50', '2019-03-01 19:16:36', 1),
(8, 8, 'MANUEL', 'ALVAREZ', 'BUCIO', 3, '12345678', '', '', '', '', '2019-02-28 03:21:45', '2019-03-01 19:16:30', 1),
(9, 9, 'JOSE', 'ALVAREZ', 'BUCIO', 2, '12345678', '', '', '', '', '2019-02-28 03:22:24', '2019-03-01 19:16:13', 1),
(10, 9, 'ALBERTO', 'ALVAREZ', 'BUCIO', 3, '12345678', '', '', '', '', '2019-02-28 03:23:02', '2019-03-01 19:16:23', 1),
(11, 10, 'ANTONIO', 'BUCIO', 'ALVAREZ', 2, '12345678', '', '', '', '', '2019-02-28 03:23:48', '2019-03-01 19:16:18', 1),
(12, 10, 'MIGUEL', 'ABURTO', 'ALVAREZ', 2, '12345678', '8c0720d95f2e4553c22bbd156b238080', '2019-03-01 17:17:17', '84711ff389f62287b1a3d3c6f903322c', '2019-03-01 17:22:17', '2019-02-28 03:24:03', '2019-03-02 05:12:17', 1);

-- --------------------------------------------------------

--
-- Table structure for table `Transferencias`
--

CREATE TABLE `Transferencias` (
  `Id_transferencia` int(11) NOT NULL,
  `origen` int(11) NOT NULL,
  `destino` int(11) NOT NULL,
  `Id_trabajador` int(11) NOT NULL,
  `Id_producto` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `enable` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Transferencias`
--

INSERT INTO `Transferencias` (`Id_transferencia`, `origen`, `destino`, `Id_trabajador`, `Id_producto`, `cantidad`, `fecha`, `create_date`, `update_date`, `enable`) VALUES
(19, 8, 9, 8, 12, 10, '2019-02-28 00:07:26', '2019-02-28 06:07:26', '2019-02-28 06:07:26', 1),
(20, 8, 9, 8, 13, 10, '2019-02-28 00:07:26', '2019-02-28 06:07:26', '2019-02-28 06:07:26', 1),
(21, 9, 10, 10, 12, 3, '2019-02-28 00:21:40', '2019-02-28 06:21:40', '2019-02-28 06:21:40', 1),
(22, 9, 10, 10, 13, 1, '2019-02-28 00:21:40', '2019-02-28 06:21:40', '2019-02-28 06:21:40', 1),
(23, 9, 10, 10, 12, 3, '2019-02-28 00:26:46', '2019-02-28 06:26:46', '2019-02-28 06:26:46', 1),
(24, 9, 10, 10, 13, 1, '2019-02-28 00:26:46', '2019-02-28 06:26:46', '2019-02-28 06:26:46', 1),
(25, 8, 9, 10, 12, 7, '2019-02-28 00:36:39', '2019-02-28 06:36:39', '2019-02-28 06:36:39', 1),
(26, 8, 9, 10, 13, 8, '2019-02-28 00:36:39', '2019-02-28 06:36:39', '2019-02-28 06:36:39', 1),
(27, 9, 10, 10, 12, 7, '2019-02-28 00:38:14', '2019-02-28 06:38:14', '2019-02-28 06:38:14', 1),
(28, 9, 10, 10, 13, 8, '2019-02-28 00:38:14', '2019-02-28 06:38:14', '2019-02-28 06:38:14', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `Id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(800) NOT NULL,
  `role` varchar(50) NOT NULL,
  `enabled` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Ventas`
--

CREATE TABLE `Ventas` (
  `Id_venta` int(11) NOT NULL,
  `Id_suc` int(11) NOT NULL,
  `Id_trabajador` int(11) NOT NULL,
  `Id_producto` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `enable` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Ventas`
--

INSERT INTO `Ventas` (`Id_venta`, `Id_suc`, `Id_trabajador`, `Id_producto`, `cantidad`, `total`, `fecha`, `create_date`, `update_date`, `enable`) VALUES
(23, 9, 9, 12, 2, 0, '2019-02-28 00:07:50', '2019-02-28 06:07:50', '2019-02-28 06:07:50', 1),
(24, 9, 9, 13, 2, 0, '2019-02-28 00:07:50', '2019-02-28 06:07:50', '2019-02-28 06:07:50', 1),
(25, 10, 11, 12, 1, 0, '2019-02-28 00:29:32', '2019-02-28 06:29:32', '2019-02-28 06:29:32', 1),
(26, 10, 12, 13, 5, 0, '2019-02-28 23:49:27', '2019-03-01 05:49:27', '2019-03-01 05:49:27', 1),
(27, 10, 12, 13, 2, 0, '2019-02-28 23:51:27', '2019-03-01 05:51:27', '2019-03-01 05:51:27', 1),
(28, 10, 12, 12, 2, 0, '2019-03-01 16:59:00', '2019-03-01 22:59:00', '2019-03-01 22:59:00', 1),
(29, 10, 12, 12, 2, 0, '2019-03-01 17:00:49', '2019-03-01 23:00:49', '2019-03-01 23:00:49', 1),
(30, 10, 12, 12, 2, 0, '2019-03-01 16:29:55', '2019-03-02 04:29:55', '2019-03-02 04:29:55', 1),
(31, 10, 12, 12, 2, 0, '2019-03-01 16:31:14', '2019-03-02 04:31:14', '2019-03-02 04:31:14', 1),
(32, 10, 12, 13, 1, 0, '2019-03-01 16:35:48', '2019-03-02 04:35:48', '2019-03-02 04:35:48', 1),
(33, 10, 12, 13, 1, 0, '2019-03-01 16:37:52', '2019-03-02 04:37:52', '2019-03-02 04:37:52', 1),
(34, 10, 12, 13, 1, 0, '2019-03-01 16:47:28', '2019-03-02 04:47:28', '2019-03-02 04:47:28', 1),
(35, 10, 12, 13, 1, 0, '2019-03-01 16:48:34', '2019-03-02 04:48:34', '2019-03-02 04:48:34', 1),
(36, 10, 12, 13, 1, 0, '2019-03-01 16:49:14', '2019-03-02 04:49:14', '2019-03-02 04:49:14', 1),
(37, 10, 12, 13, 1, 0, '2019-03-01 16:59:58', '2019-03-02 04:59:58', '2019-03-02 04:59:58', 1),
(38, 10, 12, 13, 1, 0, '2019-03-01 17:12:47', '2019-03-02 05:12:47', '2019-03-02 05:12:47', 1),
(39, 10, 12, 12, 1, 0, '2019-03-01 17:12:47', '2019-03-02 05:12:47', '2019-03-02 05:12:47', 1),
(40, 10, 12, 13, 1, 0, '2019-03-01 17:14:26', '2019-03-02 05:14:26', '2019-03-02 05:14:26', 1),
(41, 10, 12, 12, 1, 0, '2019-03-01 17:14:26', '2019-03-02 05:14:26', '2019-03-02 05:14:26', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Almacen`
--
ALTER TABLE `Almacen`
  ADD PRIMARY KEY (`Id_almacen`),
  ADD KEY `unionsuc` (`Id_suc`);

--
-- Indexes for table `Compras`
--
ALTER TABLE `Compras`
  ADD PRIMARY KEY (`Id_compra`);

--
-- Indexes for table `Productos`
--
ALTER TABLE `Productos`
  ADD PRIMARY KEY (`Id_producto`);

--
-- Indexes for table `Proveedores`
--
ALTER TABLE `Proveedores`
  ADD PRIMARY KEY (`Id_proveedor`);

--
-- Indexes for table `Tienda`
--
ALTER TABLE `Tienda`
  ADD PRIMARY KEY (`Id_suc`);

--
-- Indexes for table `Tipo_trabajador`
--
ALTER TABLE `Tipo_trabajador`
  ADD PRIMARY KEY (`Id_tipo`);

--
-- Indexes for table `Trabajadores`
--
ALTER TABLE `Trabajadores`
  ADD PRIMARY KEY (`Id_trabajador`),
  ADD KEY `tipotrabajador` (`Id_tipo`),
  ADD KEY `tiendarelacion` (`Id_suc`);

--
-- Indexes for table `Transferencias`
--
ALTER TABLE `Transferencias`
  ADD PRIMARY KEY (`Id_transferencia`),
  ADD KEY `uniontienda` (`origen`),
  ADD KEY `uniontiendadestino` (`destino`),
  ADD KEY `unionproductos` (`Id_producto`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `Ventas`
--
ALTER TABLE `Ventas`
  ADD PRIMARY KEY (`Id_venta`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Almacen`
--
ALTER TABLE `Almacen`
  MODIFY `Id_almacen` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `Compras`
--
ALTER TABLE `Compras`
  MODIFY `Id_compra` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `Productos`
--
ALTER TABLE `Productos`
  MODIFY `Id_producto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `Proveedores`
--
ALTER TABLE `Proveedores`
  MODIFY `Id_proveedor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `Tienda`
--
ALTER TABLE `Tienda`
  MODIFY `Id_suc` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `Tipo_trabajador`
--
ALTER TABLE `Tipo_trabajador`
  MODIFY `Id_tipo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `Trabajadores`
--
ALTER TABLE `Trabajadores`
  MODIFY `Id_trabajador` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `Transferencias`
--
ALTER TABLE `Transferencias`
  MODIFY `Id_transferencia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Ventas`
--
ALTER TABLE `Ventas`
  MODIFY `Id_venta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
