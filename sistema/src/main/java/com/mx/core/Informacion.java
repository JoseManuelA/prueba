package com.mx.core;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.core.Application;

import com.mx.rest.CompraService;
import com.mx.rest.ProductoService;
import com.mx.rest.ProveedorService;
import com.mx.rest.ResporteService;
import com.mx.rest.TiendaService;
import com.mx.rest.TrabajadorService;
import com.mx.rest.TransferenciaService;
import com.mx.rest.VentaService;
import com.mx.rest.oauth2Service;



public class Informacion extends Application{

    private Set<Object> singletons = new HashSet<Object>();
    
    public Informacion() {
        singletons.add(new TiendaService());
        singletons.add(new TrabajadorService());
        singletons.add(new ProductoService());
        singletons.add(new ProveedorService());
        singletons.add(new CompraService());
        singletons.add(new VentaService());
        singletons.add(new TransferenciaService());
        singletons.add(new ResporteService());
        singletons.add(new oauth2Service());
        
    }
 
    @Override
    public Set<Object> getSingletons() {
        return singletons;
    }

}