package com.mx.entidades;

import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Tienda")
public class Tienda {
	
	@Id
	@Column(name="Id_suc")
	private long Id_suc;
	
	@Column(name="nombre")
	private String nombre;
	
	@Column(name="colonia")
	private String colonia;
	
	@Column(name="calle")
	private String calle;
	
	@Column(name="cp")
	private String cp;
	
	@Column(name="numero")
	private String numero;
	
	@Column(name="telefono")
	private String telefono;
	
	@Column(name="matriz")
	private long matriz;
	
	@Column(name="create_date")
	private String create_date;
	
	@Column(name="update_date")
	private String update_date;
	
	@Column(name="enable")
	private boolean enable;
	
	public Tienda() {
		// TODO Auto-generated constructor stub
	}

	public long getId_suc() {
		return Id_suc;
	}

	public void setId_suc(long id_suc) {
		Id_suc = id_suc;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getColonia() {
		return colonia;
	}

	public void setColonia(String colonia) {
		this.colonia = colonia;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getCp() {
		return cp;
	}

	public void setCp(String cp) {
		this.cp = cp;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public long getMatriz() {
		return matriz;
	}

	public void setMatriz(long matriz) {
		this.matriz = matriz;
	}

	public String getCreate_date() {
		return create_date;
	}

	public void setCreate_date(String String) {
		this.create_date = String;
	}

	public String getUpdate_date() {
		return update_date;
	}

	public void setUpdate_date(String String) {
		this.update_date = String;
	}

	public boolean isEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	@Override
	public String toString() {
		return "Tienda [Id_suc=" + Id_suc + ", nombre=" + nombre + ", colonia=" + colonia + ", calle=" + calle + ", cp="
				+ cp + ", numero=" + numero + ", telefono=" + telefono + ", matriz=" + matriz + ", create_date="
				+ create_date + ", update_date=" + update_date + ", enable=" + enable + "]";
	}
	
	

}
