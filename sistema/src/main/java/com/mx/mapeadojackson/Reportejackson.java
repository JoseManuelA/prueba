package com.mx.mapeadojackson;

import com.mx.entidades.Compras;

import java.util.List;

public class Reportejackson {
	private long Id_suc;
	private long cantidad;
	private long valorcompra;
	private long valorventa;
	private String fechainicio;
	private String fechafin;
	private List<Compras> compras;
	private long total;
	
	public Reportejackson() {
		
	}
	
	public long getId_suc() {
		return Id_suc;
	}
	
	public void setId_suc(long id_suc) {
		Id_suc = id_suc;
	}
	
	public long getCantidad() {
		return cantidad;
	}
	
	public void setCantidad(long cantidad) {
		this.cantidad = cantidad;
	}
	
	public long getValorcompra() {
		return valorcompra;
	}
	
	public void setValorcompra(long valorcompra) {
		this.valorcompra = valorcompra;
	}
	
	public long getValorventa() {
		return valorventa;
	}
	
	public void setValorventa(long valorventa) {
		this.valorventa = valorventa;
	}
	
	public String getFechainicio() {
		return fechainicio;
	}
	
	public void setFechainicio(String fechainicio) {
		this.fechainicio = fechainicio;
	}
	
	public String getFechafin() {
		return fechafin;
	}
	
	public void setFechafin(String fechafin) {
		this.fechafin = fechafin;
	}
	
	public List<Compras> getCompras() {
		return compras;
	}
	
	public void setCompras(List<Compras> compras) {
		this.compras = compras;
	}
	
	public long getTotal() {
		return total;
	}
	
	public void setTotal(long total) {
		this.total = total;
	}

}
