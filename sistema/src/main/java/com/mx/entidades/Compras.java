package com.mx.entidades;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Compras")
public class Compras {
	
	@Id
	@Column(name="Id_compra")
	private long Id_compra;
	
	@Column(name="Id_proveedor")
	private long Id_proveedor;

	@Column(name="Id_producto")
	private long Id_producto;
	
	@Column(name="Id_suc")
	private long Id_suc;
	
	@Column(name="cantidad")
	private long cantidad;
	
	@Column(name="precio_compra")
	private long precio_compra;
	
	@Column(name="precio_venta")
	private long precio_venta;
	
	@Column(name="fecha")
	private String fecha;
	
	@Column(name="create_date")
	private String create_date;
	
	@Column(name="update_date")
	private String update_date;
	
	@Column(name="enable")
	private boolean enable;
	

	public Compras() {
		// TODO Auto-generated constructor stub
	}


	public long getId_compra() {
		return Id_compra;
	}


	public void setId_compra(long id_compra) {
		Id_compra = id_compra;
	}


	public long getId_proveedor() {
		return Id_proveedor;
	}


	public void setId_proveedor(long id_proveedor) {
		Id_proveedor = id_proveedor;
	}


	public long getId_producto() {
		return Id_producto;
	}


	public void setId_producto(long id_producto) {
		Id_producto = id_producto;
	}


	public long getId_suc() {
		return Id_suc;
	}


	public void setId_suc(long id_suc) {
		Id_suc = id_suc;
	}


	public long getCantidad() {
		return cantidad;
	}


	public void setCantidad(long cantidad) {
		this.cantidad = cantidad;
	}


	public long getPrecio_compra() {
		return precio_compra;
	}


	public void setPrecio_compra(long precio_compra) {
		this.precio_compra = precio_compra;
	}


	public long getPrecio_venta() {
		return precio_venta;
	}


	public void setPrecio_venta(long precio_venta) {
		this.precio_venta = precio_venta;
	}


	public String getFecha() {
		return fecha;
	}


	public void setFecha(String fecha) {
		this.fecha = fecha;
	}


	public String getCreate_date() {
		return create_date;
	}


	public void setCreate_date(String create_date) {
		this.create_date = create_date;
	}


	public String getUpdate_date() {
		return update_date;
	}


	public void setUpdate_date(String update_date) {
		this.update_date = update_date;
	}


	public boolean isEnable() {
		return enable;
	}


	public void setEnable(boolean enable) {
		this.enable = enable;
	}


	@Override
	public String toString() {
		return "Compras [Id_compra=" + Id_compra + ", Id_proveedor=" + Id_proveedor + ", Id_producto=" + Id_producto
				+ ", Id_suc=" + Id_suc + ", cantidad=" + cantidad + ", precio_compra=" + precio_compra
				+ ", precio_venta=" + precio_venta + ", fecha=" + fecha + ", create_date=" + create_date
				+ ", update_date=" + update_date + ", enable=" + enable + "]";
	}
	
	

}
