package com.mx.mapeadojackson;


public class Ventajackson {
	
	private long Id_venta;
	private long Id_suc;
	private long Id_trabajador;
	private long Id_producto;
	private long cantidad;
	private long total;
	private String fecha;
	private String create_date;
	private String update_date;
	private boolean enable;

	public Ventajackson() {
		// TODO Auto-generated constructor stub
	}

	public long getId_venta() {
		return Id_venta;
	}

	public void setId_venta(long id_venta) {
		Id_venta = id_venta;
	}

	public long getId_suc() {
		return Id_suc;
	}

	public void setId_suc(long id_suc) {
		Id_suc = id_suc;
	}

	public long getId_trabajador() {
		return Id_trabajador;
	}

	public void setId_trabajador(long id_trabajador) {
		Id_trabajador = id_trabajador;
	}

	public long getId_producto() {
		return Id_producto;
	}

	public void setId_producto(long id_producto) {
		Id_producto = id_producto;
	}

	public long getCantidad() {
		return cantidad;
	}

	public void setCantidad(long cantidad) {
		this.cantidad = cantidad;
	}

	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getCreate_date() {
		return create_date;
	}

	public void setCreate_date(String create_date) {
		this.create_date = create_date;
	}

	public String getUpdate_date() {
		return update_date;
	}

	public void setUpdate_date(String update_date) {
		this.update_date = update_date;
	}

	public boolean isEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	@Override
	public String toString() {
		return "Ventajackson [Id_venta=" + Id_venta + ", Id_suc=" + Id_suc + ", Id_trabajador=" + Id_trabajador
				+ ", Id_producto=" + Id_producto + ", cantidad=" + cantidad + ", total=" + total + ", fecha=" + fecha
				+ ", create_date=" + create_date + ", update_date=" + update_date + ", enable=" + enable + "]";
	}

	

	
}
