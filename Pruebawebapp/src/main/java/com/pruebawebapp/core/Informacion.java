package com.pruebawebapp.core;

import java.util.HashSet;
import java.util.Set;

//import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import com.pruebawebapp.rest.SumaRestService;
import com.pruebawebapp.rest.UsuariosService;
import com.pruebawebapp.rest.DireccionesService;

//@ApplicationPath("/rest")
public class Informacion extends Application{

    private Set<Object> singletons = new HashSet<Object>();
    
    public Informacion() {
        singletons.add(new SumaRestService());
        singletons.add(new UsuariosService());
        singletons.add(new DireccionesService());
    }
 
    @Override
    public Set<Object> getSingletons() {
        return singletons;
    }

}
