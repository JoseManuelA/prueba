package com.mx.rest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mx.configuraciones.HibernateUtil;
import com.mx.entidades.Compras;
import com.mx.entidades.Transferencias;
import com.mx.entidades.Ventas;
import com.mx.mapeadojackson.Pruebatransferencia;
import com.mx.mapeadojackson.ReporteVentajackson;
import com.mx.mapeadojackson.Reportejackson;

@Path("/Reporte")
public class ResporteService implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ResporteService() {
		// TODO Auto-generated constructor stub
	}

	@GET
	@Path("/compras")
	// @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response mostrarReporte(/* Reportejackson reportejackson */) {
		Session sessionreporte = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = sessionreporte.beginTransaction();
		try {
			@SuppressWarnings("unchecked")
			List<Compras> list = sessionreporte.createCriteria(Compras.class).add(Restrictions.eq("enable", true))
					.list();
			ObjectMapper mapper = new ObjectMapper();
			String jsonStr = "";
			int contador = 0, total = 0, preciocompra = 0, precioventa = 0;

			for (Iterator iterator = list.iterator(); iterator.hasNext();) {
				Compras compras1 = (Compras) iterator.next();
				Reportejackson reportejackson = new Reportejackson();
				List<Compras> compranu = new ArrayList<Compras>();
				for (Iterator iterator2 = list.iterator(); iterator2.hasNext();) {
					Compras compras2 = (Compras) iterator2.next();

					if (compras1.getId_suc() == compras2.getId_suc()) {
						if (compras1.getFecha().equals(compras2.getFecha())) {
							compranu.add(compras2);
							total += compras2.getCantidad() * compras2.getPrecio_venta();
							contador += compras2.getCantidad();
							preciocompra += compras2.getPrecio_compra();
							precioventa += compras2.getPrecio_venta();
						}
					}
				}
				reportejackson.setCantidad(contador);
				reportejackson.setCompras(compranu);
				reportejackson.setId_suc(compras1.getId_suc());
				reportejackson.setValorcompra(preciocompra);
				reportejackson.setValorventa(precioventa);
				reportejackson.setFechainicio(compras1.getFecha());
				reportejackson.setFechafin(compras1.getFecha());
				reportejackson.setTotal(total);
				jsonStr += mapper.writerWithDefaultPrettyPrinter().writeValueAsString(reportejackson);
				contador = 0;
				preciocompra = 0;
				precioventa = 0;
				total = 0;

			}
			return Response.status(200).entity(jsonStr).build();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return Response.status(200).entity("Error... " + e).build();
		} finally {
			sessionreporte.close();
		}
	}
	
	@GET
	@Path("/compras/total/{id}/{fecha}/{fecha2}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response mostrarReporteCompras(@PathParam("id") long id, @PathParam("fecha") String fecha,
			@PathParam("fecha2") String fecha2) {
		Session sessionreporte = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = sessionreporte.beginTransaction();

		try {
			@SuppressWarnings("unchecked")
			List<Compras> listacompra = sessionreporte.createCriteria(Compras.class).add(Restrictions.eq("Id_suc", id))
					.add(Restrictions.eq("enable", true)).add(Restrictions.between("fecha", fecha, fecha2)).list();

			ObjectMapper mapper = new ObjectMapper();
			String jsonStr = "";
			long numcompras = 0, total = 0, preciocompra = 0, precioventa = 0;
			Reportejackson reportejackson = new Reportejackson();
			Compras compra1 = new Compras();
			Compras compra2 = new Compras();
			int contador =0;
			List<Compras> lista = new ArrayList<Compras>();
			
			for (int i = 0; i < listacompra.size(); i++) {
				compra1 = listacompra.get(i);
				//validar que no se pase del tamaño de la lista
				if ((i+1)<listacompra.size()) {
					compra2 = listacompra.get(i+1);
					if (compra1.getFecha().equals(compra2.getFecha())) {
						
					}else {
						contador++;
					}
					lista.add(compra1);
				}else {
					contador++;
					lista.add(compra1);
				}
				//precio_compra+=venta1.getCantidad();
				preciocompra+=compra1.getPrecio_compra()*compra1.getCantidad();
				precioventa+=compra1.getPrecio_venta()*compra1.getCantidad();
			}
			// Aqui termina
			// reportejackson.setCompras(compranu);
			reportejackson.setCantidad(contador);
	//		reportejackson.setId_suc(compras1.getId_suc());
			// reportejackson.setComprastotales(contador);
			reportejackson.setCompras(lista);
			reportejackson.setValorcompra(preciocompra);
			reportejackson.setValorventa(precioventa);
			reportejackson.setFechainicio(fecha);
			reportejackson.setFechafin(fecha2);
			reportejackson.setTotal(total);
			jsonStr += mapper.writerWithDefaultPrettyPrinter().writeValueAsString(reportejackson);
			numcompras = 0;
			total = 0;

			return Response.status(200).entity(jsonStr).build();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return Response.status(200).entity("Error... " + e).build();
		} finally {
			sessionreporte.close();
		}
	}

	
	@GET
	@Path("/compras/resumen/{id}/{fecha}/{fecha2}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response mostrarReporteResumen(@PathParam("id") long id, @PathParam("fecha") String fecha,
			@PathParam("fecha2") String fecha2) {
		Session sessionreporte = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = sessionreporte.beginTransaction();

		try {
			@SuppressWarnings("unchecked")
			List<Compras> listacompra = sessionreporte.createCriteria(Compras.class).add(Restrictions.eq("Id_suc", id))
					.add(Restrictions.eq("enable", true)).add(Restrictions.between("fecha", fecha, fecha2)).list();

			ObjectMapper mapper = new ObjectMapper();
			String jsonStr = "";
			long preciocompra = 0, precioventa = 0;
			Reportejackson reportejackson = new Reportejackson();
			Compras compra1 = new Compras();
			Compras compra2 = new Compras();
			int contador =0;
			List<Compras> lista = new ArrayList<Compras>();
			
			for (int i = 0; i < listacompra.size(); i++) {
				compra1 = listacompra.get(i);
				//validar que no se pase del tamaño de la lista
				if ((i+1)<listacompra.size()) {
					compra2 = listacompra.get(i+1);
					if (compra1.getFecha().equals(compra2.getFecha())) {
						
					}else {
						contador++;
					}
					lista.add(compra1);
				}else {
					contador++;
					lista.add(compra1);
				}
				//precio_compra+=venta1.getCantidad();
				preciocompra+=compra1.getPrecio_compra()*compra1.getCantidad();
				precioventa+=compra1.getPrecio_venta()*compra1.getCantidad();
			}
			// Aqui termina
			// reportejackson.setCompras(compranu);
			reportejackson.setCantidad(contador);
			reportejackson.setId_suc(id);
			//reportejackson.setComprastotales(contador);
			reportejackson.setValorcompra(preciocompra);
			reportejackson.setValorventa(precioventa);
			reportejackson.setFechainicio(fecha);
			reportejackson.setFechafin(fecha2);
			reportejackson.setTotal(precioventa-preciocompra);
			jsonStr += mapper.writerWithDefaultPrettyPrinter().writeValueAsString(reportejackson);
			
			
			return Response.status(200).entity(jsonStr).build();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return Response.status(200).entity("Error... " + e).build();
		} finally {
			sessionreporte.close();
		}
	}
	

	// REPORTE DE LAS TRANSFERENCIAS EN RESUMEN
	@GET
	@Path("/transferencia/resumen/{id}/{fecha}/{fecha2}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response mostrarReporteTransfer(@PathParam("id") long id, @PathParam("fecha") String fecha,
			@PathParam("fecha2") String fecha2) {
		Session sessionreporte = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = sessionreporte.beginTransaction();
		try {

			int transferE = 0;
			int transferR = 0;
			ObjectMapper mapper = new ObjectMapper();
			@SuppressWarnings("unchecked")
			List<Transferencias> listTransferencia = sessionreporte.createCriteria(Transferencias.class)
					.add(Restrictions.eq("enable", true)).add(Restrictions.between("fecha", fecha, fecha2)).list();
			String jsonStr = "";
			if (listTransferencia != null) {
				Pruebatransferencia pruebatransferencia = new Pruebatransferencia();
				Transferencias transferencias, transferencias2 = new Transferencias();
				@SuppressWarnings("unused")
				List<Transferencias> listtransfer = new ArrayList<Transferencias>();
				int contador=0;
				for (int i = 0; i < listTransferencia.size(); i++) {
					transferencias = listTransferencia.get(i);
					//validar que no se pase del tamaño de la lista
					if ((i+1)<listTransferencia.size()) {
						transferencias2 = listTransferencia.get(i+1);
						if (transferencias.getFecha().equals(transferencias2.getFecha())) {
							if (transferencias.getOrigen() == id) {
								// Transferencias enviadas
								transferE++;

							}
							if (transferencias.getDestino() == id) {
								// Transferencias recibidas
								transferR++;
							}
							// listtransfer.add(transferencias2);
						}else {
							contador++;
						}
						//lista.add(transferencias);
					}else {
						contador++;
						//lista.add(transferencias);
					}
				}
				
				pruebatransferencia.setOrigen(id);
				pruebatransferencia.setTransferenciaE(transferE);
				pruebatransferencia.setTransferenciaR(transferR);
				// pruebatransferencia.setTransferencias(listtransfer);
				transferE = 0;
				transferR = 0;
				jsonStr += mapper.writerWithDefaultPrettyPrinter().writeValueAsString(pruebatransferencia);
				return Response.status(200).entity(jsonStr).build();
			} else {
				return Response.status(200).entity("No se puede realizar esta operacion").build();
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return Response.status(200).entity("Error... " + e).build();
		} finally {
			sessionreporte.close();
		}
	}

	// REPORTE CON DESGLOSE DE LAS TRANSFERENCIAS
	@GET
	@Path("/transferencia/todo/{id}/{fecha}/{fecha2}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response mostrarReporteTransferDes(@PathParam("id") long id, @PathParam("fecha") String fecha,
			@PathParam("fecha2") String fecha2) {
		Session sessionreporte = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = sessionreporte.beginTransaction();
		try {

			int transferE = 0;
			int transferR = 0;
			ObjectMapper mapper = new ObjectMapper();
			@SuppressWarnings("unchecked")
			List<Transferencias> listTransferencia = sessionreporte.createCriteria(Transferencias.class)
					.add(Restrictions.eq("enable", true)).add(Restrictions.between("fecha", fecha, fecha2)).list();
			String jsonStr = "";

			Pruebatransferencia pruebatransferencia = new Pruebatransferencia();
			Transferencias transferencias, transferencias2 = new Transferencias();
			List<Transferencias> listtransfer = new ArrayList<Transferencias>();
			int contador=0;
			for (int i = 0; i < listTransferencia.size(); i++) {
				transferencias = listTransferencia.get(i);
				//validar que no se pase del tamaño de la lista
				if ((i+1)<listTransferencia.size()) {
					transferencias2 = listTransferencia.get(i+1);
					if (transferencias.getFecha().equals(transferencias2.getFecha())) {
						if (transferencias.getOrigen() == id) {
							// Transferencias enviadas
							transferE++;

						}
						if (transferencias.getDestino() == id) {
							// Transferencias recibidas
							transferR++;
						}
						listtransfer.add(transferencias2);
					}else {
						contador++;
					}
					listtransfer.add(transferencias);
				}else {
					contador++;
				}
			}
			pruebatransferencia.setOrigen(id);
			pruebatransferencia.setTransferenciaE(transferE);
			pruebatransferencia.setTransferenciaR(transferR);
			pruebatransferencia.setTransferencias(listtransfer);
			transferE = 0;
			transferR = 0;
			jsonStr += mapper.writerWithDefaultPrettyPrinter().writeValueAsString(pruebatransferencia);

			return Response.status(200).entity(jsonStr).build();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return Response.status(200).entity("Error... " + e).build();
		} finally {
			sessionreporte.close();
		}
	}

	@GET
	@Path("/ventas/resumen/{id}/{fecha}/{fecha2}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response mostrarVentasTransfer(@PathParam("id") long id, @PathParam("fecha") String fecha,
			@PathParam("fecha2") String fecha2) {
		Session sessionreporte = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = sessionreporte.beginTransaction();
		try {

			long precio_compra = 0, precio_venta = 0,cont=0;
			ObjectMapper mapper = new ObjectMapper();

			@SuppressWarnings("unchecked")
			List<Ventas> listVentas = sessionreporte.createCriteria(Ventas.class).add(Restrictions.eq("enable", true))
					.add(Restrictions.eq("Id_suc", id)).add(Restrictions.between("fecha", fecha, fecha2)).list();

			String jsonStr = "";

			if (listVentas != null) {

				ReporteVentajackson reporteVentajackson = new ReporteVentajackson();
				reporteVentajackson.setId_suc(id);

				Ventas venta = null, venta2 = new Ventas();
				@SuppressWarnings("unused")
				List<Ventas> listtransfer = new ArrayList<Ventas>();
				int contador=0;
				
				for (int i = 0; i < listVentas.size(); i++) {
					venta = listVentas.get(i);
					//validar que no se pase del tamaño de la lista
					if ((i+1)<listVentas.size()) {
						venta2 = listVentas.get(i+1);
						if (venta.getFecha().equals(venta2.getFecha())) {
							
							
							listtransfer.add(venta2);
						}else {
							contador++;
						}
						listtransfer.add(venta);
					}else {
						contador++;
					}
				}
				
				reporteVentajackson.setCantidad(contador);
				jsonStr += mapper.writerWithDefaultPrettyPrinter().writeValueAsString(reporteVentajackson);
				return Response.status(200).entity(jsonStr).build();
			} else {
				return Response.status(200).entity("No se puede realizar esta operacion").build();
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return Response.status(200).entity("Error... " + e).build();
		} finally {
			sessionreporte.close();
		}
	}
	
	@GET
	@Path("/ventas/todo/{id}/{fecha}/{fecha2}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response mostrarVentasTodo(@PathParam("id") long id, @PathParam("fecha") String fecha,
			@PathParam("fecha2") String fecha2) {
		Session sessionreporte = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = sessionreporte.beginTransaction();
		try {
			long precio_compra = 0, precio_venta = 0,cont=0;
			ObjectMapper mapper = new ObjectMapper();

			@SuppressWarnings("unchecked")
			List<Ventas> listVentas = sessionreporte.createCriteria(Ventas.class).add(Restrictions.eq("enable", true))
					.add(Restrictions.eq("Id_suc", id)).add(Restrictions.between("fecha", fecha, fecha2)).list();

			String jsonStr = "";

			if (listVentas != null) {

				ReporteVentajackson reporteVentajackson = new ReporteVentajackson();
				reporteVentajackson.setId_suc(id);

				Ventas venta = null, venta2 = new Ventas();
				@SuppressWarnings("unused")
				List<Ventas> listtransfer = new ArrayList<Ventas>();
				int contador=0;
				
				for (int i = 0; i < listVentas.size(); i++) {
					venta = listVentas.get(i);
					//validar que no se pase del tamaño de la lista
					if ((i+1)<listVentas.size()) {
						venta2 = listVentas.get(i+1);
						if (venta.getFecha().equals(venta2.getFecha())) {
							
							
							listtransfer.add(venta2);
						}else {
							contador++;
						}
						listtransfer.add(venta);
					}else {
						contador++;
					}
				}
				//reporteVentajackson.setPrecio_compra(precio_compra);
				//reporteVentajackson.setPrecio_venta(precio_venta);
				reporteVentajackson.setCantidad(contador);
				reporteVentajackson.setVentas(listtransfer);

				jsonStr += mapper.writerWithDefaultPrettyPrinter().writeValueAsString(reporteVentajackson);
				return Response.status(200).entity(jsonStr).build();
			} else {
				return Response.status(200).entity("No se puede realizar esta operacion").build();
			}


		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return Response.status(200).entity("Error... " + e).build();
		} finally {
			sessionreporte.close();
		}
	}
}
