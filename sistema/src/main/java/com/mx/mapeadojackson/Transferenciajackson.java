package com.mx.mapeadojackson;

import javax.persistence.Column;

public class Transferenciajackson {
	
	private long Id_transferencia;
	private long origen;
	private long destino;
	private long Id_producto;
	private long Id_trabajador;
	private long cantidad;
	private String fecha;
	private String create_date;
	private String update_date;
	private boolean enable;

	public Transferenciajackson() {
		// TODO Auto-generated constructor stub
	}

	public long getId_transferencia() {
		return Id_transferencia;
	}

	public void setId_transferencia(long id_transferencia) {
		Id_transferencia = id_transferencia;
	}

	public long getOrigen() {
		return origen;
	}

	public void setOrigen(long origen) {
		this.origen = origen;
	}

	public long getDestino() {
		return destino;
	}

	public void setDestino(long destino) {
		this.destino = destino;
	}

	public long getId_producto() {
		return Id_producto;
	}

	public void setId_producto(long id_producto) {
		Id_producto = id_producto;
	}

	public long getCantidad() {
		return cantidad;
	}

	public void setCantidad(long cantidad) {
		this.cantidad = cantidad;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getCreate_date() {
		return create_date;
	}

	public void setCreate_date(String create_date) {
		this.create_date = create_date;
	}

	public String getUpdate_date() {
		return update_date;
	}

	public void setUpdate_date(String update_date) {
		this.update_date = update_date;
	}

	public boolean isEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	public long getId_trabajador() {
		return Id_trabajador;
	}

	public void setId_trabajador(long id_trabajador) {
		Id_trabajador = id_trabajador;
	}

	@Override
	public String toString() {
		return "Transferenciajackson [Id_transferencia=" + Id_transferencia + ", origen=" + origen + ", destino="
				+ destino + ", Id_producto=" + Id_producto + ", Id_trabajador=" + Id_trabajador + ", cantidad="
				+ cantidad + ", fecha=" + fecha + ", create_date=" + create_date + ", update_date=" + update_date
				+ ", enable=" + enable + "]";
	}

	
	

}
