package com.pruebawebapp.entidadesjson;

public class Usuariosjson {

	public Usuariosjson() {
		// TODO Auto-generated constructor stub
	}

	private int Ids;

	private String nombre;

	private String apellido;

	private int edad;

	public int getIds() {
		return Ids;
	}

	public void setIds(int ids) {
		Ids = ids;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	@Override
	public String toString() {
		return "Usuarios [Ids=" + Ids + ", nombre=" + nombre + ", apellido=" + apellido + ", edad=" + edad + "]";
	}
	
}
