package com.pruebawebapp.rest;

import java.io.Serializable;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.hibernate.Session;
import org.hibernate.Transaction;
import com.pruebawebapp.trabajo.HibernateUtil;
import com.pruebawebapp.entidades.Direccion;;

@Path("/Direcciones")
public class DireccionesService implements Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@GET
	@Path("/direccion/{calle}/{colonia}/{id}")
	@Produces("text/plain")
	public String Mostarmensaje(@PathParam("calle") String calle,@PathParam("colonia") String colonia,@PathParam("id") int id) {
		Session sessiondir = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = sessiondir.beginTransaction();
		Direccion dire = new Direccion();
		dire.setColonia(colonia);
		dire.setCalle(calle);
		dire.setId_usr(id);

		try {
			// Se guardan los datos con save
			sessiondir.save(dire);
			tx.commit();

		} catch (RuntimeException e) {
			e.printStackTrace();
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			sessiondir.close();
		}

		System.out.println("Insertada direccion");
		//return (long) dire.getId();
		return "Calle: " + calle + " Colonia: " + colonia + " Id: " + id;

	}



}
