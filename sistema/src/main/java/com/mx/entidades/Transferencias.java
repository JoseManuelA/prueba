package com.mx.entidades;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Transferencias")
public class Transferencias {
	
	@Id
	@Column(name="Id_transferencia")
	private long Id_transferencia;

	@Column(name="origen")
	private long origen;

	@Column(name="destino")
	private long destino;

	@Column(name="Id_producto")
	private long Id_producto;
	
	@Column(name="Id_trabajador")
	private long Id_trabajador;

	@Column(name="cantidad")
	private long cantidad;

	@Column(name="fecha")
	private String fecha;

	@Column(name="create_date")
	private String create_date;

	@Column(name="update_date")
	private String update_date;

	@Column(name="enable")
	private boolean enable;

	public Transferencias() {
		// TODO Auto-generated constructor stub
	}

	public long getId_transferencia() {
		return Id_transferencia;
	}

	public void setId_transferencia(long id_transferencia) {
		Id_transferencia = id_transferencia;
	}

	public long getOrigen() {
		return origen;
	}

	public void setOrigen(long origen) {
		this.origen = origen;
	}

	public long getDestino() {
		return destino;
	}

	public void setDestino(long destino) {
		this.destino = destino;
	}

	public long getId_producto() {
		return Id_producto;
	}

	public void setId_producto(long id_producto) {
		Id_producto = id_producto;
	}

	public long getCantidad() {
		return cantidad;
	}

	public void setCantidad(long cantidad) {
		this.cantidad = cantidad;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getCreate_date() {
		return create_date;
	}

	public void setCreate_date(String create_date) {
		this.create_date = create_date;
	}

	public String getUpdate_date() {
		return update_date;
	}

	public void setUpdate_date(String update_date) {
		this.update_date = update_date;
	}

	public boolean isEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	public long getId_trabajador() {
		return Id_trabajador;
	}

	public void setId_trabajador(long id_trabajador) {
		Id_trabajador = id_trabajador;
	}

	@Override
	public String toString() {
		return "Transferencias [Id_transferencia=" + Id_transferencia + ", origen=" + origen + ", destino=" + destino
				+ ", Id_producto=" + Id_producto + ", Id_trabajador=" + Id_trabajador + ", cantidad=" + cantidad
				+ ", fecha=" + fecha + ", create_date=" + create_date + ", update_date=" + update_date + ", enable="
				+ enable + "]";
	}


	
	

}
