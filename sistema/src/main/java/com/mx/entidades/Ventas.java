package com.mx.entidades;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Ventas")
public class Ventas {
	
	@Id
	@Column(name="Id_venta")
	private long Id_venta;
	
	@Column(name="Id_suc")
	private long Id_suc;
	
	@Column(name="Id_trabajador")
	private long Id_trabajador;
	
	@Column(name="Id_producto")
	private long Id_producto;
	
	@Column(name="cantidad")
	private long cantidad;
	
	@Column(name="total")
	private long total;
	
	@Column(name="fecha")
	private String fecha;
	
	@Column(name="create_date")
	private String create_date;
	
	@Column(name="update_date")
	private String update_date;
	
	@Column(name="enable")
	private boolean enable;
	

	public Ventas() {
		// TODO Auto-generated constructor stub
	}


	public long getId_venta() {
		return Id_venta;
	}


	public void setId_venta(long id_venta) {
		Id_venta = id_venta;
	}


	public long getId_suc() {
		return Id_suc;
	}


	public void setId_suc(long id_suc) {
		Id_suc = id_suc;
	}


	public long getId_trabajador() {
		return Id_trabajador;
	}


	public void setId_trabajador(long id_trabajador) {
		Id_trabajador = id_trabajador;
	}


	public long getId_producto() {
		return Id_producto;
	}


	public void setId_producto(long id_producto) {
		Id_producto = id_producto;
	}


	public long getCantidad() {
		return cantidad;
	}


	public void setCantidad(long cantidad) {
		this.cantidad = cantidad;
	}


	public long getTotal() {
		return total;
	}


	public void setTotal(long total) {
		this.total = total;
	}


	public String getFecha() {
		return fecha;
	}


	public void setFecha(String fecha) {
		this.fecha = fecha;
	}


	public String getCreate_date() {
		return create_date;
	}


	public void setCreate_date(String create_date) {
		this.create_date = create_date;
	}


	public String getUpdate_date() {
		return update_date;
	}


	public void setUpdate_date(String update_date) {
		this.update_date = update_date;
	}


	public boolean isEnable() {
		return enable;
	}


	public void setEnable(boolean enable) {
		this.enable = enable;
	}


	@Override
	public String toString() {
		return "Ventas [Id_venta=" + Id_venta + ", Id_suc=" + Id_suc + ", Id_trabajador=" + Id_trabajador
				+ ", Id_producto=" + Id_producto + ", cantidad=" + cantidad + ", total=" + total + ", fecha=" + fecha
				+ ", create_date=" + create_date + ", update_date=" + update_date + ", enable=" + enable + "]";
	}


	

}
