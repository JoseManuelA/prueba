package com.mx.rest;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mx.configuraciones.HibernateUtil;
import com.mx.entidades.Almacen;
import com.mx.entidades.Tienda;
import com.mx.entidades.Tipo_trabajador;
import com.mx.entidades.Trabajadores;
import com.mx.entidades.Transferencias;
import com.mx.mapeadojackson.Transferenciajackson;

@Path("/Transferencia")
public class TransferenciaService implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TransferenciaService() {
		// TODO Auto-generated constructor stub
	}

	@POST
	@Path("/new/variado")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addVenta(List<Transferenciajackson> list) {
		Session sessiontransferencia;
		Transaction tx = null;
		if (list.size() > 0) {
			try {
				for (@SuppressWarnings("rawtypes")
				Iterator iterator = list.iterator(); iterator.hasNext();) {
					Transferenciajackson transferenciajackson = (Transferenciajackson) iterator.next();
					sessiontransferencia = HibernateUtil.getSessionFactory().openSession();
					tx = sessiontransferencia.beginTransaction();

					// Verificar si la tienda esat habilitada
					Tienda tiendaorigen, tiendadestino;
					tiendaorigen = (Tienda) sessiontransferencia.get(Tienda.class, transferenciajackson.getOrigen());
					tiendadestino = (Tienda) sessiontransferencia.get(Tienda.class, transferenciajackson.getDestino());
					if ((tiendaorigen != null) && (tiendaorigen.isEnable()) && (tiendadestino != null)
							&& (tiendadestino.isEnable())) {

						Trabajadores trabajador = (Trabajadores) sessiontransferencia.get(Trabajadores.class,
								transferenciajackson.getId_trabajador());
						if ((trabajador != null)) {
							Tipo_trabajador tipo = (Tipo_trabajador) sessiontransferencia.get(Tipo_trabajador.class,
									trabajador.getId_tipo());
							if (tipo != null && tipo.getPuesto().equals("Almacen")) {
								Transferencias transferencias = new Transferencias();
								transferencias.setOrigen(transferenciajackson.getOrigen());
								transferencias.setDestino(transferenciajackson.getDestino());
								transferencias.setCantidad(transferenciajackson.getCantidad());
								transferencias.setId_producto(transferenciajackson.getId_producto());
								transferencias.setId_trabajador(transferenciajackson.getId_trabajador());
								transferencias.setFecha(ObtenerUTC());
								transferencias.setCreate_date(ObtenerUTC());
								transferencias.setUpdate_date(ObtenerUTC());
								transferencias.setEnable(true);
								sessiontransferencia.save(transferencias);

								Almacen almacen1 = (Almacen) sessiontransferencia.createCriteria(Almacen.class)
										.add(Restrictions.eq("Id_producto", transferenciajackson.getId_producto()))
										.add(Restrictions.eq("Id_suc", tiendaorigen.getId_suc())).uniqueResult();
								if ((almacen1 != null)
										&& (almacen1.getCantidad() - transferenciajackson.getCantidad() > 0)) {
									almacen1.setCantidad(almacen1.getCantidad() - transferenciajackson.getCantidad());
									almacen1.setUpdate_date(ObtenerUTC());
									sessiontransferencia.update(almacen1);

									// Aumentar el almacen destino
									Almacen almacen2 = (Almacen) sessiontransferencia.createCriteria(Almacen.class)
											.add(Restrictions.eq("Id_producto", transferenciajackson.getId_producto()))
											.add(Restrictions.eq("Id_suc", tiendadestino.getId_suc())).uniqueResult();
									if (almacen2 != null) {
										almacen2.setCantidad(
												almacen2.getCantidad() + transferenciajackson.getCantidad());
										almacen2.setUpdate_date(ObtenerUTC());
										sessiontransferencia.update(almacen2);
									} else {
										// Crear el registro
										almacen2 = new Almacen();
										almacen2.setId_suc(transferenciajackson.getDestino());
										almacen2.setId_producto(transferenciajackson.getId_producto());
										almacen2.setCantidad(transferenciajackson.getCantidad());
										almacen2.setCreate_date(ObtenerUTC());
										almacen2.setUpdate_date(ObtenerUTC());
										almacen2.setEnable(true);
										sessiontransferencia.save(almacen2);
									}
								} else {
									return Response.status(200).entity("error en almacen").build();
								}
								tx.commit();
							} else {
								return Response.status(200).entity("error en tipo").build();
							}
						} else {
							return Response.status(200).entity("error en Trabajador").build();
						}
					} else {
						return Response.status(200).entity("error en Tienda").build();
					}
				}
				return Response.status(200).entity("Operacion exitosa!").build();
			} catch (RuntimeException e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
		return Response.status(200).entity("error en la operacion").build();
	}

	@POST
	@Path("/new/")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addVenta(Transferenciajackson transferenciajackson) {
		Session sessiontransferencia;
		Transaction tx = null;
		if (transferenciajackson != null) {
			try {
				sessiontransferencia = HibernateUtil.getSessionFactory().openSession();
				tx = sessiontransferencia.beginTransaction();
				Tienda tiendaorigen, tiendadestino;
				tiendaorigen = (Tienda) sessiontransferencia.get(Tienda.class, transferenciajackson.getOrigen());
				tiendadestino = (Tienda) sessiontransferencia.get(Tienda.class, transferenciajackson.getDestino());
				if ((tiendaorigen != null) && (tiendaorigen.isEnable()) && (tiendadestino != null)
						&& (tiendadestino.isEnable())) {

					Trabajadores trabajador = (Trabajadores) sessiontransferencia.get(Trabajadores.class,
							transferenciajackson.getId_trabajador());
					if ((trabajador != null)) {
						Tipo_trabajador tipo = (Tipo_trabajador) sessiontransferencia.get(Tipo_trabajador.class,
								trabajador.getId_tipo());
						if (tipo != null && tipo.getPuesto().equals("Almacen")) {
							Transferencias transferencias = new Transferencias();
							transferencias.setOrigen(transferenciajackson.getOrigen());
							transferencias.setDestino(transferenciajackson.getDestino());
							transferencias.setCantidad(transferenciajackson.getCantidad());
							transferencias.setId_producto(transferenciajackson.getId_producto());
							transferencias.setId_trabajador(transferenciajackson.getId_trabajador());
							transferencias.setFecha(ObtenerUTC());
							transferencias.setCreate_date(ObtenerUTC());
							transferencias.setUpdate_date(ObtenerUTC());
							transferencias.setEnable(true);
							sessiontransferencia.save(transferencias);

							Almacen almacen1 = (Almacen) sessiontransferencia.createCriteria(Almacen.class)
									.add(Restrictions.eq("Id_producto", transferenciajackson.getId_producto()))
									.add(Restrictions.eq("Id_suc", tiendaorigen.getId_suc())).uniqueResult();
							if ((almacen1 != null)
									&& (almacen1.getCantidad() - transferenciajackson.getCantidad() > 0)) {
								almacen1.setCantidad(almacen1.getCantidad() - transferenciajackson.getCantidad());
								almacen1.setUpdate_date(ObtenerUTC());
								sessiontransferencia.update(almacen1);

								// Aumentar el almacen destino
								Almacen almacen2 = (Almacen) sessiontransferencia.createCriteria(Almacen.class)
										.add(Restrictions.eq("Id_producto", transferenciajackson.getId_producto()))
										.add(Restrictions.eq("Id_suc", tiendadestino.getId_suc())).uniqueResult();
								if (almacen2 != null) {
									almacen2.setCantidad(almacen2.getCantidad() + transferenciajackson.getCantidad());
									almacen2.setUpdate_date(ObtenerUTC());
									sessiontransferencia.update(almacen2);
								} else {
									// Crear el registro
									almacen2 = new Almacen();
									almacen2.setId_suc(transferenciajackson.getDestino());
									almacen2.setId_producto(transferenciajackson.getId_producto());
									almacen2.setCantidad(transferenciajackson.getCantidad());
									almacen2.setCreate_date(ObtenerUTC());
									almacen2.setUpdate_date(ObtenerUTC());
									almacen2.setEnable(true);
									sessiontransferencia.save(almacen2);
								}
							} else {
								return Response.status(200).entity("error en almacen").build();
							}
							tx.commit();
						} else {
							return Response.status(200).entity("error en tipo").build();
						}
					} else {
						return Response.status(200).entity("error en Trabajador").build();
					}
				} else {
					return Response.status(200).entity("error en Tienda").build();
				}
				return Response.status(200).entity("Operacion exitosa!").build();
			} catch (RuntimeException e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
		return Response.status(200).entity("error en la operacion").build();
	}

	@DELETE
	@Path("/delete/{id}")
	public Response deleteTienda(@PathParam("id") long id) {
		Session sessiontransferencia = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = sessiontransferencia.beginTransaction();
		try {
			Transferencias transferencia = (Transferencias) sessiontransferencia.get(Transferencias.class, id);
			if (transferencia != null) {
				if (transferencia.isEnable()) {
					// Actualizar el estado de la transferencia
					transferencia.setEnable(false);
					sessiontransferencia.update(transferencia);
					Almacen almacen = (Almacen) sessiontransferencia.createCriteria(Almacen.class)
							.add(Restrictions.eq("Id_producto", transferencia.getId_producto()))
							.add(Restrictions.eq("Id_suc", transferencia.getOrigen())).uniqueResult();
					if (almacen != null) {
						almacen.setCantidad(almacen.getCantidad() + transferencia.getCantidad());
						sessiontransferencia.update(almacen);
						Almacen almacen2 = (Almacen) sessiontransferencia.createCriteria(Almacen.class)
								.add(Restrictions.eq("Id_producto", transferencia.getId_producto()))
								.add(Restrictions.eq("Id_suc", transferencia.getDestino())).uniqueResult();
						if (almacen2 != null) {
							almacen2.setCantidad(almacen2.getCantidad() - transferencia.getCantidad());
							sessiontransferencia.update(almacen2);
						} else {
							return Response.status(200).entity("error en almacen destino").build();
						}
					} else {
						return Response.status(200).entity("error en almacen origen").build();
					}

				}
				tx.commit();
			}
			return Response.status(200).entity("Operacion exitosa!").build();
		} catch (RuntimeException e) {
			// TODO: handle exception
			e.printStackTrace();
			e.printStackTrace();
			if ((tx != null) && (tx.isActive())) {
				tx.rollback();
			}
		} finally {
			sessiontransferencia.close();
		}
		return Response.status(200).entity("error en la operacion").build();
	}

	@GET
	@Path("/search/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response searchTransferencia(@PathParam("id") long id) {
		Session sessionventa = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = sessionventa.beginTransaction();
		try {

			@SuppressWarnings("unchecked")
			Transferencias transferencias = (Transferencias) sessionventa.get(Transferencias.class, id);
			ObjectMapper mapper = new ObjectMapper();
			String jsonStr = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(transferencias);

			return Response.status(200).entity(jsonStr).build();
		} catch (RuntimeException e) {
			e.printStackTrace();
			if ((tx != null) && (tx.isActive())) {
				tx.rollback();
			}
			return Response.status(200).entity("Error: " + e).build();
		}
		catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return Response.status(200).entity("Error..").build();
	}

	@GET
	@Path("/search")
	@Produces(MediaType.APPLICATION_JSON)
	public Response searchTransferenciaTotal() {
		Session sessionventa = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = sessionventa.beginTransaction();
		try {
			@SuppressWarnings("unchecked")
			List<Transferencias> transferencias = (List<Transferencias>) sessionventa
					.createCriteria(Transferencias.class).list();
			ObjectMapper mapper = new ObjectMapper();
			String jsonStr = "";
			for (@SuppressWarnings("rawtypes")
			Iterator iterator = transferencias.iterator(); iterator.hasNext();) {
				Transferencias transferencias2 = (Transferencias) iterator.next();
				jsonStr += mapper.writerWithDefaultPrettyPrinter().writeValueAsString(transferencias2);
			}
			return Response.status(200).entity(jsonStr).build();
		} catch (RuntimeException e) {
			e.printStackTrace();
			if ((tx != null) && (tx.isActive())) {
				tx.rollback();
			}
			return Response.status(200).entity("Error: " + e).build();
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return Response.status(200).entity("Error en la operacion").build();
	}

	public static String ObtenerUTC() {
		final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
		final String utcTime = sdf.format(new Date());
		return utcTime;
	}

	public String ObtenerGMT() {
		final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();  
		return ""+sdf.format(new Date());
	}
}
