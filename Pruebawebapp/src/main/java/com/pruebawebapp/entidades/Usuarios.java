package com.pruebawebapp.entidades;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import com.pruebawebapp.entidades.Direccion;


@Entity
@Table(name="Usuarios")
public class Usuarios {


	@Id
	@Column(name="Id")
	private int Ids;
	
	@Column(name="nombre")
	private String nombre;
	
	@Column(name="apellido")
	private String apellido;

	@Column(name="edad")
	private int edad;
	
	@OneToMany
	private List<Direccion> direcciones;
	
	
	public Usuarios() {
		
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public void setIds(int ids) {
		Ids = ids;
	}

	
	public int getIds() {
		return Ids;
	}

	
	public List<Direccion> getDirecciones() {
		return direcciones;
	}

	public void setDirecciones(List<Direccion> direcciones) {
		this.direcciones = direcciones;
	}

	@Override
	public String toString() {
		return "Usuarios [Ids=" + Ids + ", nombre=" + nombre + ", apellido=" + apellido + ", edad=" + edad
				+ ", direccion=" + direcciones + "]";
	}

	
}
