package com.mx.entidades;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Proveedores")
public class Proveedores {
	@Id
	@Column(name="Id_proveedor")
	private long Id_proveedor;
	
	@Column(name="nombre")
	private String nombre;

	@Column(name="telefono")
	private String telefono;

	@Column(name="create_date")
	private String create_date;

	@Column(name="update_date")
	private String update_date;

	@Column(name="enable")
	private boolean enable;
	
	public Proveedores() {
		// TODO Auto-generated constructor stub
	}

	public long getId_proveedor() {
		return Id_proveedor;
	}

	public void setId_proveedor(long id_proveedor) {
		Id_proveedor = id_proveedor;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getcreate_date() {
		return create_date;
	}

	public void setcreate_date(String create_date) {
		this.create_date = create_date;
	}

	public String getUpdate_date() {
		return update_date;
	}

	public void setUpdate_date(String update_date) {
		this.update_date = update_date;
	}

	public boolean isEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	@Override
	public String toString() {
		return "Proveedores [Id_proveedor=" + Id_proveedor + ", nombre=" + nombre + ", telefono=" + telefono
				+ ", create_date=" + create_date + ", update_date=" + update_date + ", enable=" + enable + "]";
	}
	
	

}
