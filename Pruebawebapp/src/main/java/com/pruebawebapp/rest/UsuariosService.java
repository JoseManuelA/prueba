package com.pruebawebapp.rest;


import java.io.IOException;
import java.io.Serializable;
import java.util.Iterator;
import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.pruebawebapp.entidades.Usuarios;
import com.pruebawebapp.trabajo.HibernateUtil;


@Path("/Usuarios")
public class UsuariosService implements Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
/*
 * 	@GET
	@Path("/usuario/{id}")
	@Produces("text/plain")
	public String mostrarUsuario(@PathParam("id") String id) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Criteria cr = session.createCriteria(Usuarios.class)
				.add(Restrictions.eq("Ids", Integer.parseInt(id)));
		cr.list();
		String contenido="";
		try {
			List<Usuarios> list = cr.list();
			for (Iterator iterator = (list).iterator(); iterator.hasNext();) {
				Usuarios usr = (Usuarios) iterator.next();
				contenido+=" Nombre: "+usr.getNombre()+" apellido: "+usr.getApellido()+" edad: "+usr.getEdad()+" \n";
			}

		} catch (RuntimeException e) {
			e.printStackTrace();

		}
		
		return contenido;
	}
 */
	@GET
	@Path("/usuario/{id}")
	@Produces("text/plain")
	public String mostrarUsuario(@PathParam("id") int id) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Criteria cr = session.createCriteria(Usuarios.class)
				.add(Restrictions.eq("Id", id));
		cr.list();
		String contenido="";
		ObjectMapper objectMapper = new ObjectMapper();
    	//Set pretty printing of json
    	objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
    	String arrayToJson="";
		try {
			List<Usuarios> listapersona = cr.list();
			
			arrayToJson = objectMapper.writeValueAsString(listapersona);
		

		} catch (RuntimeException e) {
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		return arrayToJson;
	}

	@GET
	@Path("/usuario/{nombre}/{apellido}/{edad}")
	@Produces("text/plain")
	public String addUsuario(@PathParam("nombre") String nombre, @PathParam("apellido") String apellido,
			@PathParam("edad") String edad) {
		Session sessiondir = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = sessiondir.beginTransaction();
		Usuarios usr = new Usuarios();
		usr.setNombre(nombre);
		usr.setApellido(apellido);
		usr.setEdad(Integer.parseInt(edad));

		try {
			// Se guardan los datos con save
			sessiondir.save(usr);
			tx.commit();

		} catch (RuntimeException e) {
			e.printStackTrace();
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			sessiondir.close();
		}

		System.out.println("Insertada direccion");
		return "Nombre: " + nombre + " apellido: " + apellido + " edad: " + edad;
	}

	@PUT
	@Path("/usuario/{x1}/{x2}")
	public String ussaalgo(@PathParam("x1") String x1, @PathParam("x2") String x2) {
		System.out.print("Se uno delete"+x1+" "+x2);
		return "Se uno delete";
		}

	@DELETE
	   @Path("/usuario/delete/{id}")
	   public String removeBook(@PathParam("id") String id){
		System.out.print("Delete id: "+id);
		return "Se uno delete"+id;
		}

}
