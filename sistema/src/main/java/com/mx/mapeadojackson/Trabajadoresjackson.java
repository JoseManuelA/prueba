package com.mx.mapeadojackson;

import javax.persistence.Column;

public class Trabajadoresjackson {
	
	private long Id_trabajador;
	private long Id_suc;
	private String nombre_trabajador;
	private String apellidop_trabajador;
	private String apellidom_trabajador;
	private String password;
	private String token_access;
	private String fecha_access;
	private String token_refresh;
	private String fecha_refresh;
	private String confirmacion_password;
	private long Id_tipo;
	private String create_date;
	private String update_date;
	private boolean enable;
	
	


	public String getToken_access() {
		return token_access;
	}




	public void setToken_access(String token_access) {
		this.token_access = token_access;
	}




	public String getFecha_access() {
		return fecha_access;
	}




	public void setFecha_access(String fecha_access) {
		this.fecha_access = fecha_access;
	}




	public String getToken_refresh() {
		return token_refresh;
	}




	public void setToken_refresh(String token_refresh) {
		this.token_refresh = token_refresh;
	}




	public String getFecha_refresh() {
		return fecha_refresh;
	}




	public void setFecha_refresh(String fecha_refresh) {
		this.fecha_refresh = fecha_refresh;
	}




	public long getId_trabajador() {
		return Id_trabajador;
	}




	public void setId_trabajador(long id_trabajador) {
		Id_trabajador = id_trabajador;
	}




	public long getId_suc() {
		return Id_suc;
	}




	public void setId_suc(long id_suc) {
		Id_suc = id_suc;
	}




	public String getNombre_trabajador() {
		return nombre_trabajador;
	}




	public void setNombre_trabajador(String nombre_trabajador) {
		this.nombre_trabajador = nombre_trabajador;
	}




	public String getApellidop_trabajador() {
		return apellidop_trabajador;
	}




	public void setApellidop_trabajador(String apellidop_trabajador) {
		this.apellidop_trabajador = apellidop_trabajador;
	}




	public String getApellidom_trabajador() {
		return apellidom_trabajador;
	}




	public void setApellidom_trabajador(String apellidom_trabajador) {
		this.apellidom_trabajador = apellidom_trabajador;
	}




	public String getPassword() {
		return password;
	}




	public void setPassword(String password) {
		this.password = password;
	}




	public String getConfirmacion_password() {
		return confirmacion_password;
	}




	public void setConfirmacion_password(String confirmacion_password) {
		this.confirmacion_password = confirmacion_password;
	}




	public long getId_tipo() {
		return Id_tipo;
	}




	public void setId_tipo(long id_tipo) {
		Id_tipo = id_tipo;
	}




	public String getCreate_date() {
		return create_date;
	}




	public void setCreate_date(String create_date) {
		this.create_date = create_date;
	}




	public String getUpdate_date() {
		return update_date;
	}




	public void setUpdate_date(String update_date) {
		this.update_date = update_date;
	}




	public boolean isEnable() {
		return enable;
	}




	public void setEnable(boolean enable) {
		this.enable = enable;
	}




	public Trabajadoresjackson() {
		// TODO Auto-generated constructor stub
	}
	

}
