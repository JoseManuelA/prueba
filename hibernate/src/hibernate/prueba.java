package hibernate;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import com.sun.istack.logging.Logger;

import hibernate.Usuarios;
import hibernate.direccion;

public class prueba {
	private final static Logger log = Logger.getLogger(prueba.class);
	//private Session session = null;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//createAndStoreEvent(5, "Beto", "Herrera", 25);
		//criteria();
		//Direccion("lomas de morelia","lomas de las fuentes");
		criteriaInnerJoin();
	}

	public prueba() {
		/*createAndStoreEvent(2, "Juan", "Perez", 31);
		// listEvents();
		HibernateUtil.getSessionFactory().close();
		*/
	}
	private static Long Direccion( String colonia, String calle) {
		Session sessiondir = HibernateUtil.getSessionFactory().openSession();
		Transaction tx=sessiondir.beginTransaction();
		direccion dire = new direccion();
		dire.setColonia(colonia);
		dire.setCalle(calle);

		try {
			sessiondir.save(dire);
			tx.commit();
			
		} catch (RuntimeException e) {
			e.printStackTrace();
			if(tx!=null) {
				tx.rollback();
			}
		} finally {
			sessiondir.close();
		}
		
		log.info("Insertada direccion");
		return (long) dire.getId();
	}

	private static Long createAndStoreEvent(int id, String nombre, String apellido, int num) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction tx=session.beginTransaction();
		Usuarios usuario = new Usuarios();
		usuario.setId(id);
		usuario.setNombre(nombre);
		usuario.setApellido(apellido);
		usuario.setEdad(num);

		try {
			session.save(usuario);
			tx.commit();
			
		} catch (RuntimeException e) {
			e.printStackTrace();
			if(tx!=null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
		
		log.info("Insertado:");
		return (long) usuario.getId();
	}
	//List results = new List<>;
public static void criteriaInnerJoin() {
		
		Session session = HibernateUtil.getSessionFactory().openSession();   
		Criteria cr = session.createCriteria(direccion.class,"dirc");
		cr.add(Restrictions.eq("dire.Id", 2));
		//cr.add(nameCriterion);
		cr.list();
		
		//		.setFetchMode("Usuarios", FetchMode.JOIN);
		
		
		List<direccion> list = cr.list();
		for (Iterator iterator = (list).iterator(); iterator.hasNext();){
			direccion users = (direccion) iterator.next(); 
            System.out.println("Id: " + users.getId()); 
            System.out.println(" Name: " + users.getCalle()); 
            System.out.println(" Last Name: " + users.getColonia());
            //System.out.println(" Edad: " + users.getEdad());
			
		}
		   
		   //Criterion nameCriterion = Restrictions.eq("dire.Id", 2);
		   //criteria.add(nameCriterion);
		   //criteria.list();
			
		
		/*
		Criteria cr = session.createCriteria(Usuarios.class,"usr")
		.createAlias("Usuarios", "usr")
		//.createAlias("direccion", "dirc")
		.add(Restrictions.like("usr.nombre", "J%"));
		*/
		//cr.list();
		
		//cr.createAlias("direccion", "direccion");
		//cr.add(Restrictions.like("direccion.Id",1));
		//System.out.print(books.get(0));
		
		//cr.add(Restrictions.like("nombre", "J%"));
		//cr.add(Restrictions.gt("edad",22));
		//cr.addOrder(Order.asc("edad"));
		//cr.setProjection(Projections.max("edad"));
		//cr.add(Restrictions.gt("edad",22));
		
		/*List<Usuarios> results = cr.list();
		if(results!=null){			
			
			for (Iterator iterator = (results).iterator(); iterator.hasNext();){
				Usuarios users = (Usuarios) iterator.next(); 
	            System.out.print("Id: " + users.getId()); 
	            System.out.print(" Name: " + users.getNombre()); 
	            System.out.print(" Last Name: " + users.getApellido());
	            System.out.println(" Edad: " + users.getEdad());
				
			}
		}
		*/
}
	
	
	
	public static void criteria() {
		
		Session session = HibernateUtil.getSessionFactory().openSession();
		
		
		Criteria cr = session.createCriteria(Usuarios.class);
		
		//cr.add(Restrictions.like("nombre", "J%"));
		//cr.add(Restrictions.gt("edad",22));
		cr.addOrder(Order.asc("edad"));
		//cr.setProjection(Projections.max("edad"));
		//cr.add(Restrictions.gt("edad",22));
		List<Usuarios> results = cr.list();
		if(results!=null){			
			
			for (Iterator iterator = (results).iterator(); iterator.hasNext();){
				Usuarios users = (Usuarios) iterator.next(); 
	            System.out.print("Id: " + users.getId()); 
	            System.out.print(" Name: " + users.getNombre()); 
	            System.out.print(" Last Name: " + users.getApellido());
	            System.out.println(" Edad: " + users.getEdad());
				
			}
		}
		/*
		for (Usuarios usuarios : results) {
			System.out.print("Id: " + results); 
			//System.out.print("Name: " + results.getNombre()); 
			//System.out.println("Last Name: " + results.getApellido());
		}
		/*
		 * 
		 */
		/*
		for (Iterator iterator = (results).iterator(); iterator.hasNext();){
			Usuarios employee = (Usuarios) iterator.next(); 
            System.out.print("Id: " + employee.getId()); 
            System.out.print("Name: " + employee.getNombre()); 
            System.out.println("Last Name: " + employee.getApellido()); 
         }
		*/
		/*
		Transaction tx=session.beginTransaction();
		Usuarios cat = new Usuarios();
		cat.setId(4);
		cat.setNombre("juaquin");
		cat.setApellido("algo");
		
		resultado =  session.createCriteria(Usuarios.class)
			    .add( Restrictions.like("name", "Sara%")).list();
		
		
		resultado = session.createCriteria(Usuarios.class).add( Example.create(cat)).list();
		prettyPrinter(res);
		
		*/
	}
	
	/*
	 * private List<Usuarios> listEvents() { Session session =
	 * HibernateUtil.getSessionFactory().getCurrentSession();
	 * session.beginTransaction(); List<Usuarios> result =
	 * (List<Usuarios>)session.createQuery("from Event").list();
	 * session.getTransaction().commit(); for (Usuarios evento : result) {
	 * log.info("Leido: "+evento); }
	 * 
	 * return result;
	 * 
	 * }
	 */

}